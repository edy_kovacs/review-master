﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Licenta.Domain.Utilities;

namespace Licenta.Data.Api.Handler
{
    class AuthenticatedHttpClientHandler : HttpClientHandler
    {
        private readonly Func<string> getToken;

        public AuthenticatedHttpClientHandler(Func<string> getToken)
        {
            this.getToken = getToken ?? throw new ArgumentNullException(nameof(getToken));
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var auth = request.Headers.Authorization;
            if (auth != null)
            {
                var token = getToken();
                Logger.Instance.Debug($"Token: {token}");
                request.Headers.Authorization = new AuthenticationHeaderValue(auth.Scheme, token);
            }

            return await base.SendAsync(request, cancellationToken).ConfigureAwait(false);
        }
    }
}
