﻿using System;
using Licenta.Data.Models;
using Licenta.Domain.Models;

namespace Licenta.Data.Api.Authentication
{
    public class AuthenticationApiService : BaseApiService<IAuthenticationApi>
    {
        public static IAuthenticationApi GetService()
        {
            return GetAuthorizedApiService(typeof(IAuthenticationApi), ServerConfig.GetBaseUrl());
        }

        public static IObservable<UserDataModel> Register(CredentialsDataModel credentials)
        {
            return GetService().Register(credentials);
        }

        public static IObservable<UserDataModel> Login(CredentialsDataModel credentials)
        {
            return GetService().Login(credentials);
        }

        public static IObservable<UserDataModel> LoginWithFacebook(string authToken)
        {
            return GetService().LoginWithFacebook(authToken);
        }

        public static IObservable<UserDataModel> UpdateProfile(CredentialsDataModel credentials, string authToken)
        {
            return GetService().UpdateProfile(credentials, authToken);
        }

        public static IObservable<object> Logout(string authToken)
        {
            return GetApiService(typeof(IAuthenticationApi), ServerConfig.GetBaseUrl()).Logout(authToken);
        }
    }
}
