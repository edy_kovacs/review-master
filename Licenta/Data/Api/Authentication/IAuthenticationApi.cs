﻿using System;
using Licenta.Data.Models;
using Licenta.Utilities;
using Refit;

namespace Licenta.Data.Api.Authentication
{
    public interface IAuthenticationApi
    {
        [Post("/signup")]
        IObservable<UserDataModel> Register([Body] CredentialsDataModel credentials);

        [Post("/login")]
        IObservable<UserDataModel> Login([Body] CredentialsDataModel credentials);

        [Post("/auth/facebook/token?access_token={authToken}")]
        IObservable<UserDataModel> LoginWithFacebook([AliasAs("authToken")] string authToken);

        [Patch("/updateProfile")]
        IObservable<UserDataModel> UpdateProfile([Body] CredentialsDataModel credentials, [Header("x-auth")] string authToken);

        [Delete("/logout")]
        IObservable<object> Logout([Header("x-auth")] string authToken);
    }
}
