﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Licenta.Data.Api.Handler;
using Licenta.Utilities;
using Refit;
using Xamarin.Essentials;

namespace Licenta.Data.Api
{
    public class BaseApiService<TApiService> where TApiService : class
    {
        private static Dictionary<string, TApiService> _apiServices = new Dictionary<string, TApiService>();
        private static Dictionary<string, TApiService> _authorizedApiServices = new Dictionary<string, TApiService>();

        public static readonly int RequestTimeout = 60;

        public static TApiService GetApiService(Type clazz, string url)
        {

            if (!_apiServices.TryGetValue(url, out TApiService service))
            {
                var httpClient = new HttpClient(new HttpLoggingHandler())
                {
                    BaseAddress = new UriBuilder(url).Uri,
                    Timeout = TimeSpan.FromSeconds(RequestTimeout)
                };
                service = RestService.For<TApiService>(httpClient);
                _apiServices.Add(url, service);
            }

            return service;
        }

        public static TApiService GetAuthorizedApiService(Type clazz, string url)
        {
            TApiService service;

            if (!_authorizedApiServices.TryGetValue(url, out service))
            {
                var httpClient = new HttpClient(new HttpLoggingHandler(innerHandler: new AuthenticatedHttpClientHandler(GetToken)))
                {
                    BaseAddress = new UriBuilder(url).Uri,
                    Timeout = TimeSpan.FromSeconds(RequestTimeout)
                };
                service = RestService.For<TApiService>(httpClient);
                _authorizedApiServices.Add(url, service);
            }

            return service;
        }

        private static string GetToken()
        {
            return Preferences.Get(Constants.RestStrings.Token, string.Empty);
        }
    }
}
