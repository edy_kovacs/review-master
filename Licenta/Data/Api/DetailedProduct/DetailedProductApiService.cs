﻿using System;
using Licenta.Data.Api.RecentSearches;
using Licenta.Data.Models;

namespace Licenta.Data.Api.DetailedProduct
{
    public class DetailedProductApiService : BaseApiService<IDetailedProductApi>
    {
        public static IDetailedProductApi GetService()
        {
            return GetApiService(typeof(IDetailedProductApi), ServerConfig.GetBaseUrl());
        }

        public static IObservable<DetailedProductDataModel> GetProduct(string productId, string authToken)
        {
            return GetService().GetProduct(productId, authToken);
        }
    }
}
