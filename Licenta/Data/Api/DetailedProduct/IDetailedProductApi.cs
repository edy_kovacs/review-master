﻿using System;
using Licenta.Data.Models;
using Refit;

namespace Licenta.Data.Api.DetailedProduct
{
    public interface IDetailedProductApi
    {
        [Get("/product/{id}")]
        IObservable<DetailedProductDataModel> GetProduct([AliasAs("id")] string productId, [Header("x-auth")] string authToken);
    }
}
