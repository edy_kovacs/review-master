﻿using System;
using System.Collections.Generic;
using Licenta.Data.Models;

namespace Licenta.Data.Api.RecentSearches
{
    public class RecentSearchesApiService : BaseApiService<IRecentSearchesApi>
    {
        public static IRecentSearchesApi GetService()
        {
            return GetApiService(typeof(IRecentSearchesApi), ServerConfig.GetBaseUrl());
        }

        public static IObservable<List<ProductDataModel>> GetRecentSearches(string authToken)
        {
            return GetService().GetRecentSearches(authToken);
        }

        public static IObservable<List<ProductDataModel>> GetSearchResults(string authToken, Dictionary<string, string> searchText)
        {
            return GetService().GetSearchResults(authToken, searchText);
        }
    }
}
