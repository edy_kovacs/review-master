﻿using System;
using System.Collections.Generic;
using Licenta.Data.Models;
using Refit;

namespace Licenta.Data.Api.RecentSearches
{
    public interface IRecentSearchesApi
    {
        [Get("/recentSearches")]
        IObservable<List<ProductDataModel>> GetRecentSearches([Header("x-auth")] string authToken);
        [Post("/search")]
        IObservable<List<ProductDataModel>> GetSearchResults([Header("x-auth")] string authToken, [Body] Dictionary<string, string> searchText);
    }
}
