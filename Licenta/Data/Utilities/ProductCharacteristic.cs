﻿using Newtonsoft.Json;

namespace Licenta.Data.Utilities
{
    public class ProductCharacteristic
    {
        [JsonProperty("_id")]
        public string Id { get; set; }
        [JsonProperty("subcategory")]
        public string Subcategory { get; set; }
        [JsonProperty("key")]
        public string Key { get; set; }
        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
