﻿using System;
using Newtonsoft.Json;

namespace Licenta.Data.Utilities
{
    public class ApiErrorDataModel
    {
        [JsonProperty("err")]
        public string ErrorMessage { get; set; }
    }
}
