﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Licenta.Data.Models
{
    public class RegionDataModel
    {
        [JsonProperty("boundingBox")]
        public object BoundingBox { get; set; }
        [JsonProperty("lines")]
        public List<LineDataModel> Lines { get; set; }
    }
}
