﻿using System;
using System.Collections.Generic;
using Licenta.Data.Utilities;
using Newtonsoft.Json;

namespace Licenta.Data.Models
{
    public class ProductDataModel
    {
        [JsonProperty("_id")]
        public string Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("photo")]
        public string PhotoURL { get; set; }
        [JsonProperty("brand")]
        public string Brand { get; set; }
        [JsonProperty("model")]
        public string Model { get; set; }
        [JsonProperty("reviews")]
        public List<string> Reviews { get; set; }
        [JsonProperty("characteristics")]
        public List<ProductCharacteristic> Characteristics { get; set; }
    }
}
