﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Licenta.Data.Models
{
    public class LineDataModel
    {
        [JsonProperty("boundingBox")]
        public object BoundingBox { get; set; }
        [JsonProperty("words")]
        public List<WordDataModel> Words { get; set; }
    }
}
