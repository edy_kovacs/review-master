﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Licenta.Data.Models
{
    public class ReviewDataModel
    {
        [JsonProperty("_id")]
        public string Id { get; set; }
        [JsonProperty("author")]
        public string Author { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("rating")]
        public string Rating { get; set; }
        [JsonProperty("dateOfReview")]
        public string DateOfReview { get; set; }
        [JsonProperty("provider")]
        public string Provider { get; set; }
        [JsonProperty("likes")]
        public List<string> Likes { get; set; }
    }
}
