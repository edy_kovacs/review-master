﻿using Newtonsoft.Json;

namespace Licenta.Data.Models
{
    public class CredentialsDataModel
    {
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("displayName")]
        public string DisplayName { get; set; }
        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
