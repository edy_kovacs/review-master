﻿using System;
using Newtonsoft.Json;

namespace Licenta.Data.Models
{
    public class WordDataModel
    {
        [JsonProperty("boundingBox")]
        public object BoundingBox { get; set; }
        [JsonProperty("text")]
        public string Text { get; set; }
    }
}
