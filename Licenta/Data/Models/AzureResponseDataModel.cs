﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Licenta.Data.Models
{
    public class AzureResponseDataModel
    {
        [JsonProperty("language")]
        public string Language { get; set; }
        [JsonProperty("textAngle")]
        public double TextAngle { get; set; }
        [JsonProperty("orientation")]
        public string Orientation { get; set; }
        [JsonProperty("regions")]
        public List<RegionDataModel> Regions { get; set; }
    }
}
