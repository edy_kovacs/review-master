﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Licenta.Data.Models
{
    public class UserDataModel
    {
        [JsonProperty("_id")]
        public string Id { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("displayName")]
        public string DisplayName { get; set; }
        [JsonProperty("photoURL")]
        public string PhotoURL { get; set; }
        [JsonProperty("photoID")]
        public string PhotoID { get; set; }
        [JsonProperty("createdAt")]
        public string CreatedDate { get; set; }
        [JsonProperty("token")]
        public string Token { get; set; }
        [JsonProperty("recentSearches")]
        public List<ProductDataModel> RecentSearches { get; set; }
    }
}
