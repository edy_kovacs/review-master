﻿using System;

namespace Licenta.Data.Repository
{
    public class BaseApiRepository<TModelConverter> where TModelConverter : class
    {
        protected TModelConverter ModelConverter { get; set; }

        public BaseApiRepository(TModelConverter converter)
        {
            ModelConverter = converter;
        }
    }
}
