﻿using System;
using System.Reactive.Linq;
using Licenta.Data.Api.DetailedProduct;
using Licenta.Data.Converters;
using Licenta.Data.Models;
using Licenta.Domain.Models;
using Licenta.Domain.Repository;

namespace Licenta.Data.Repository
{
    public class DetailedProductApiRepository : BaseApiRepository<BaseConverter<DetailedProductDataModel, DetailedProductDomainModel>>, IDetailedProductRepository
    {
        public DetailedProductApiRepository() : base(new DetailedProductModelConverter())
        {
        }

        public IObservable<DetailedProductDomainModel> FetchProduct(string productId, string authToken)
        {
            return DetailedProductApiService.GetProduct(productId, authToken).Select(element => ModelConverter.Transform(element));
        }
    }
}
