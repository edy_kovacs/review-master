﻿using System;
using System.Reactive.Linq;
using Licenta.Data.Api.Authentication;
using Licenta.Data.Converters;
using Licenta.Data.Models;
using Licenta.Domain.Models;
using Licenta.Domain.Repository;

namespace Licenta.Data.Repository
{
    public class AuthenticationApiRepository : BaseApiRepository<BaseConverter<UserDataModel, UserDomainModel>>, IAuthenticationApiRepository
    {
        private CredentialsModelConverter _credentialsModelConverter;

        public AuthenticationApiRepository() : base(new UserModelConverter())
        {
            _credentialsModelConverter = new CredentialsModelConverter();
        }

        public IObservable<UserDomainModel> Register(CredentialsDomainModel credentials)
        {
            return AuthenticationApiService.Register(_credentialsModelConverter.ReverseTransform(credentials)).Select(dataModel => ModelConverter.Transform(dataModel));
        }

        public IObservable<UserDomainModel> Login(CredentialsDomainModel credentials)
        {
            return AuthenticationApiService.Login(_credentialsModelConverter.ReverseTransform(credentials)).Select(dataModel => ModelConverter.Transform(dataModel));
        }

        public IObservable<UserDomainModel> LoginWithFacebook(string authToken)
        {
            return AuthenticationApiService.LoginWithFacebook(authToken).Select(dataModel => ModelConverter.Transform(dataModel));
        }

        public IObservable<UserDomainModel> UpdateProfile(CredentialsDomainModel credentials, string authToken)
        {
            return AuthenticationApiService.UpdateProfile(_credentialsModelConverter.ReverseTransform(credentials), authToken).Select(dataModel => ModelConverter.Transform(dataModel));
        }

        public IObservable<object> Logout(string authToken)
        {
            return AuthenticationApiService.Logout(authToken);
        }
    }
}
