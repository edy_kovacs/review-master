﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using Licenta.Data.Api.RecentSearches;
using Licenta.Data.Converters;
using Licenta.Data.Models;
using Licenta.Domain.Models;
using Licenta.Domain.Repository;

namespace Licenta.Data.Repository
{
    public class RecentSearchesApiRepository : BaseApiRepository<BaseConverter<ProductDataModel, ProductDomainModel>>, IRecentSearchesRepository
    {
        public RecentSearchesApiRepository() : base(new ProductModelConverter())
        {
        }

        public IObservable<List<ProductDomainModel>> FetchRecentSearches(string authToken)
        {
            return RecentSearchesApiService.GetRecentSearches(authToken).Select(list => list.Select(dataModel => ModelConverter.Transform(dataModel)).ToList());
        }

        public IObservable<List<ProductDomainModel>> FetchSearchResults(string authToken, Dictionary<string, string> searchText)
        {
            return RecentSearchesApiService.GetSearchResults(authToken, searchText).Select(list => list.Select(dataModel => ModelConverter.Transform(dataModel)).ToList());
        }
    }
}
