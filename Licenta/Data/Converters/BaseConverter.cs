﻿namespace Licenta.Data.Converters
{
    public abstract class BaseConverter<TDataModel, TDomainModel>
    {
        public abstract TDomainModel Transform(TDataModel convertableItem);
        public abstract TDataModel ReverseTransform(TDomainModel convertableItem);
    }
}
