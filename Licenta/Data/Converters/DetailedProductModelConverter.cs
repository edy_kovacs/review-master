﻿using System;
using System.Linq;
using Licenta.Data.Models;
using Licenta.Domain.Models;

namespace Licenta.Data.Converters
{
    public class DetailedProductModelConverter : BaseConverter<DetailedProductDataModel, DetailedProductDomainModel>
    {
        ReviewModelConverter _reviewModelConverter;

        public override DetailedProductDataModel ReverseTransform(DetailedProductDomainModel convertableItem)
        {
            throw new NotImplementedException();
        }

        public override DetailedProductDomainModel Transform(DetailedProductDataModel convertableItem)
        {
            if (convertableItem == null) return null;

            var domainModel = new DetailedProductDomainModel
            {
                Id = convertableItem.Id,
                Name = convertableItem.Name,
                PhotoURL = convertableItem.PhotoURL,
                Brand = convertableItem.Brand,
                Model = convertableItem.Model,
                Characteristics = convertableItem.Characteristics
            };

            _reviewModelConverter = new ReviewModelConverter();
            domainModel.Reviews = convertableItem.Reviews.Select((item) => _reviewModelConverter.Transform(item)).ToList();

            return domainModel;
        }
    }
}
