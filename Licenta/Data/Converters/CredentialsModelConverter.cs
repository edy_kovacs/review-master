﻿using System;
using Licenta.Data.Models;
using Licenta.Domain.Models;

namespace Licenta.Data.Converters
{
    public class CredentialsModelConverter : BaseConverter<CredentialsDataModel, CredentialsDomainModel>
    {
        public override CredentialsDataModel ReverseTransform(CredentialsDomainModel convertableItem)
        {
            if (convertableItem == null) return null;

            var dataModel = new CredentialsDataModel
            {
                Email = convertableItem.Email,
                DisplayName = convertableItem.DisplayName,
                Password = convertableItem.Password
            };

            return dataModel;
        }

        public override CredentialsDomainModel Transform(CredentialsDataModel convertableItem)
        {
            if (convertableItem == null) return null;

            var domainModel = new CredentialsDomainModel
            {
                Email = convertableItem.Email,
                DisplayName = convertableItem.DisplayName,
                Password = convertableItem.Password
            };

            return domainModel;
        }
    }
}
