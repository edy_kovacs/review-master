﻿using System;
using System.Collections.Generic;
using Licenta.Data.Models;
using Licenta.Domain.Models;

namespace Licenta.Data.Converters
{
    public class ReviewModelConverter : BaseConverter<ReviewDataModel, ReviewDomainModel>
    {
        public override ReviewDataModel ReverseTransform(ReviewDomainModel convertableItem)
        {
            throw new NotImplementedException();
        }

        public override ReviewDomainModel Transform(ReviewDataModel convertableItem)
        {
            if (convertableItem == null) return null;

            var domainModel = new ReviewDomainModel
            {
                Id = convertableItem.Id,
                Author = convertableItem.Author,
                Title = convertableItem.Title,
                Message = convertableItem.Message,
                Rating = convertableItem.Rating,
                DateOfReview = convertableItem.DateOfReview,
                Provider = convertableItem.Provider,
                Likes = convertableItem.Likes?.Count > 0 ? convertableItem.Likes : new List<string>()
            };

            return domainModel;
        }
    }
}
