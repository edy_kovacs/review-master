﻿using System;
using Licenta.Data.Models;
using Licenta.Domain.Models;

namespace Licenta.Data.Converters
{
    public class ProductModelConverter : BaseConverter<ProductDataModel, ProductDomainModel>
    {
        public override ProductDataModel ReverseTransform(ProductDomainModel convertableItem)
        {
            throw new NotImplementedException();
        }

        public override ProductDomainModel Transform(ProductDataModel convertableItem)
        {
            if (convertableItem == null) return null;

            var domainModel = new ProductDomainModel
            {
                Id = convertableItem.Id,
                Name = convertableItem.Name,
                PhotoURL = convertableItem.PhotoURL,
                Brand = convertableItem.Brand,
                Model = convertableItem.Model,
                Reviews = convertableItem.Reviews,
                Characteristics = convertableItem.Characteristics
            };

            return domainModel;
        }
    }
}
