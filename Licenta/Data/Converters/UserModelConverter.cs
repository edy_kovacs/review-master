﻿using System;
using System.Linq;
using Licenta.Data.Models;
using Licenta.Domain.Models;
using Licenta.Presentation.Utilities;

namespace Licenta.Data.Converters
{
    public class UserModelConverter : BaseConverter<UserDataModel, UserDomainModel>
    {
        public override UserDataModel ReverseTransform(UserDomainModel convertableItem)
        {
            throw new NotImplementedException();
        }

        public override UserDomainModel Transform(UserDataModel convertableItem)
        {
            if (convertableItem == null) return null;

            var productModelConverter = new ProductModelConverter();

            var domainModel = new UserDomainModel
            {
                Id = convertableItem.Id,
                Name = convertableItem.DisplayName,
                PhotoURL = convertableItem.PhotoURL,
                CreatedDate = convertableItem.CreatedDate,
                Email = convertableItem.Email,
                Token = convertableItem.Token
            };

            domainModel.RecentSearches = convertableItem.RecentSearches.Select(item => productModelConverter.Transform(item)).WhereNotNull().ToList();

            return domainModel;
        }
    }
}
