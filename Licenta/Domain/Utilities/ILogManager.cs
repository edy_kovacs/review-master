﻿using System;

namespace Licenta.Domain.Utilities
{
    public interface ILogManager
    {
        ILogger GetLog();
        string GetLogsFileContent();
    }
}
