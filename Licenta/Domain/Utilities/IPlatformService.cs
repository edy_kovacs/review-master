﻿using System;
namespace Licenta.Domain.Utilities
{
    public interface IPlatformService
    {
        char GetDirectoryPathSeparator();
    }
}
