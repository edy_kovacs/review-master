﻿using System;
using System.Threading.Tasks;

namespace Licenta.Domain.Utilities
{
    public static class TaskExtensions
    {
        public static void Forget(this Task task)
        {
            task.ContinueWith(
                t => { Logger.Instance.Warn(t.Exception.Message); },
                TaskContinuationOptions.OnlyOnFaulted);
        }
    }
}
