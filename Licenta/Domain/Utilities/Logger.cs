﻿using System;
using Xamarin.Forms;

namespace Licenta.Domain.Utilities
{
    public sealed class Logger
    {
        //private static readonly Lazy<Logger> lazy = new Lazy<Logger>(() => new Logger());
        public static Logger Instance { get { return new Logger(); } }

        private static ILogger _logger;
        private static ILogManager _loggerManager;

        public Logger()
        {
            _loggerManager = DependencyService.Get<ILogManager>();



            _logger = _loggerManager.GetLog();
        }

        public string GetLogsFileContent()
        {
            return _loggerManager.GetLogsFileContent();
        }

        public void Trace(string text, [System.Runtime.CompilerServices.CallerFilePath] string callerFilePath = "", params object[] args)
        {
            var fullText = $"[{GetCallerName(callerFilePath)}] {text}";
            _logger.Trace(fullText, args);
        }
        public void Debug(string text, [System.Runtime.CompilerServices.CallerFilePath] string callerFilePath = "", params object[] args)
        {
            var fullText = $"[{GetCallerName(callerFilePath)}] {text}";
            _logger.Debug(fullText, args);
        }
        public void Info(string text, [System.Runtime.CompilerServices.CallerFilePath] string callerFilePath = "", params object[] args)
        {
            var fullText = $"[{GetCallerName(callerFilePath)}] {text}";
            _logger.Info(fullText, args);
        }
        public void Warn(string text, [System.Runtime.CompilerServices.CallerFilePath] string callerFilePath = "", params object[] args)
        {
            var fullText = $"[{GetCallerName(callerFilePath)}] {text}";
            _logger.Warn(fullText, args);
        }
        public void Error(string text, [System.Runtime.CompilerServices.CallerFilePath] string callerFilePath = "", params object[] args)
        {
            var fullText = $"[{GetCallerName(callerFilePath)}] {text}";
            _logger.Error(fullText, args);
        }
        public void Fatal(string text, [System.Runtime.CompilerServices.CallerFilePath] string callerFilePath = "", params object[] args)
        {
            var fullText = $"[{GetCallerName(callerFilePath)}] {text}";
            _logger.Fatal(fullText, args);
        }

        private static string GetCallerName([System.Runtime.CompilerServices.CallerFilePath] string callerFilePath = "")
        {
            string fileName = callerFilePath;
            try
            {
                var separator = DependencyService.Get<IPlatformService>().GetDirectoryPathSeparator().ToString();
                if (fileName.Contains(separator))
                {
                    fileName = fileName.Substring(fileName.LastIndexOf(separator, StringComparison.CurrentCultureIgnoreCase) + 1);
                }

                return fileName;
            }
            catch (Exception ex)
            {
                Logger.Instance.Error(ex.Message);
                return "/";
            }
        }
    }
}
