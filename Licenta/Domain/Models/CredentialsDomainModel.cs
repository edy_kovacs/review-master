﻿using System;
namespace Licenta.Domain.Models
{
    public class CredentialsDomainModel
    {
        public string Email { get; set; }
        public string DisplayName { get; set; }
        public string Password { get; set; }
    }
}
