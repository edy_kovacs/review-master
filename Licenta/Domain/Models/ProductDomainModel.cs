﻿using System;
using System.Collections.Generic;
using Licenta.Data.Utilities;

namespace Licenta.Domain.Models
{
    public class ProductDomainModel
    {
        public string PhotoURL { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public List<string> Reviews { get; set; }
        public List<ProductCharacteristic> Characteristics { get; set; }
    }
}
