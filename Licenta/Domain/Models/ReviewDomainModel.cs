﻿using System;
using System.Collections.Generic;

namespace Licenta.Domain.Models
{
    public class ReviewDomainModel
    {
        public string Id { get; set; }
        public string Author { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string Rating { get; set; }
        public string DateOfReview { get; set; }
        public string Provider { get; set; }
        public List<string> Likes { get; set; }
    }
}
