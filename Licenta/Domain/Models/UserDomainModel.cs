﻿using System;
using System.Collections.Generic;

namespace Licenta.Domain.Models
{
    public class UserDomainModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public string PhotoURL { get; set; }
        public string CreatedDate { get; set; }
        public List<ProductDomainModel> RecentSearches { get; set; }
    }
}
