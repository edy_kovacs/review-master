﻿using System;
using System.Collections.Generic;
using Licenta.Domain.Models;

namespace Licenta.Domain.Repository
{
    public interface IRecentSearchesRepository
    {
        IObservable<List<ProductDomainModel>> FetchRecentSearches(string authToken);
        IObservable<List<ProductDomainModel>> FetchSearchResults(string authToken, Dictionary<string, string> searchText);
    }
}
