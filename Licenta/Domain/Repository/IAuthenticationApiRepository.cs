﻿using System;
using Licenta.Domain.Models;

namespace Licenta.Domain.Repository
{
    public interface IAuthenticationApiRepository
    {
        IObservable<UserDomainModel> Login(CredentialsDomainModel credentials);
        IObservable<UserDomainModel> LoginWithFacebook(string authToken);
        IObservable<UserDomainModel> Register(CredentialsDomainModel credentials);
        IObservable<UserDomainModel> UpdateProfile(CredentialsDomainModel credentials, string authToken);
        IObservable<object> Logout(string authToken);
    }
}
