﻿using System;
using Licenta.Domain.Models;

namespace Licenta.Domain.Repository
{
    public interface IDetailedProductRepository
    {
        IObservable<DetailedProductDomainModel> FetchProduct(string productId, string authToken);
    }
}
