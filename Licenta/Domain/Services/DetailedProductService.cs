﻿using System;
using Licenta.Domain.Models;
using Licenta.Domain.Repository;
using Licenta.Domain.Utilities;

namespace Licenta.Domain.Services
{
    public class DetailedProductService : Service
    {
        public IDetailedProductRepository Repository { get; set; }

        public DetailedProductService(IDetailedProductRepository repository)
        {
            Repository = repository;
        }

        public void GetProduct(IObserver<DetailedProductDomainModel> observer, string productId, string authToken)
        {
            ExecuteAsync(Repository.FetchProduct(productId, authToken), observer).Forget();
        }
    }
}
