﻿using System;
using System.Collections.Generic;
using Licenta.Domain.Models;
using Licenta.Domain.Repository;
using Licenta.Domain.Utilities;

namespace Licenta.Domain.Services
{
    public class RecentSearchesService : Service
    {
        public IRecentSearchesRepository Repository { get; set; }

        public RecentSearchesService(IRecentSearchesRepository repository)
        {
            Repository = repository;
        }

        public void FetchRecentSearches(IObserver<List<ProductDomainModel>> observer, string authToken)
        {
            ExecuteAsync(Repository.FetchRecentSearches(authToken), observer).Forget();
        }

        public void FetchSearchResults(IObserver<List<ProductDomainModel>> observer, string authToken, Dictionary<string, string> searchText)
        {
            ExecuteAsync(Repository.FetchSearchResults(authToken, searchText), observer).Forget();
        }
    }
}
