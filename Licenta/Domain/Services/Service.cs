﻿using System;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Threading.Tasks;

namespace Licenta.Domain.Services
{
    public class Service
    {
        private const int RetryNo = 1;

        protected async Task ExecuteAsync<T>(IObservable<T> observable, IObserver<T> observer, bool tokenRequired = true)
        {
            //TODO verify if token is valid

            observable.SubscribeOn(Scheduler.Default)
                      .ObserveOn(Scheduler.CurrentThread)
                      .Retry(RetryNo)
                      .Subscribe(observer);
        }
    }
}
