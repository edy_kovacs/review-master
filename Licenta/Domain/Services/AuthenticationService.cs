﻿using System;
using Licenta.Domain.Models;
using Licenta.Domain.Repository;
using Licenta.Domain.Utilities;

namespace Licenta.Domain.Services
{
    public class AuthenticationService : Service
    {
        private IAuthenticationApiRepository _repository;

        public AuthenticationService(IAuthenticationApiRepository repository)
        {
            _repository = repository;
        }

        public void Register(IObserver<UserDomainModel> observer, CredentialsDomainModel credentialsDomainModel)
        {
            ExecuteAsync(_repository.Register(credentialsDomainModel), observer).Forget();
        }

        public void Login(IObserver<UserDomainModel> observer, CredentialsDomainModel credentialsDomainModel)
        {
            ExecuteAsync(_repository.Login(credentialsDomainModel), observer).Forget();
        }

        public void LoginWithFacebook(IObserver<UserDomainModel> observer, string authToken)
        {
            ExecuteAsync(_repository.LoginWithFacebook(authToken), observer).Forget();
        }

        public void UpdateProfile(IObserver<UserDomainModel> observer, CredentialsDomainModel credentialsDomainModel, string authToken)
        {
            ExecuteAsync(_repository.UpdateProfile(credentialsDomainModel, authToken), observer).Forget();
        }

        public void Logout(IObserver<object> observer, string authToken)
        {
            ExecuteAsync(_repository.Logout(authToken), observer).Forget();
        }
    }
}
