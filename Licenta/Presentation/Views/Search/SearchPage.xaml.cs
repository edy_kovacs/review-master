﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading;
using Licenta.Presentation.ViewModels.Search;
using Xamarin.Forms;

namespace Licenta.Presentation.Views.Search
{
    public partial class SearchPage : BasePage
    {
        public SearchPageViewModel ViewModel
        {
            get => BindingContext as SearchPageViewModel;
            set => BindingContext = value;
        }

        public SearchPage()
        {
            InitializeComponent();

            ViewModel = new SearchPageViewModel();

            Observable
                .FromEventPattern<TextChangedEventArgs>(NavigationEntry, nameof(NavigationEntry.TextChanged))
                .Select(t => t.EventArgs.NewTextValue)
                .Throttle(TimeSpan.FromMilliseconds(500))
                .ObserveOn(SynchronizationContext.Current)
                .Where(str => ViewModel.SearchSuggestionsCommand.CanExecute(str))
                .Subscribe(ViewModel.SearchSuggestionsCommand.Execute);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            NavigationEntry.Focus();
        }
    }
}
