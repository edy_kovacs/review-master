﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Licenta.Presentation.ViewModels.Search;
using Licenta.Presentation.Views.CustomViews;
using Licenta.Utilities;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Xamarin.Forms;

namespace Licenta.Presentation.Views.Search
{
    public partial class OcrSearchPage : BasePage
    {
        public OcrSearchPageViewModel ViewModel
        {
            get => BindingContext as OcrSearchPageViewModel;
            set => BindingContext = value;
        }

        public OcrSearchPage()
        {
            InitializeComponent();

            ViewModel = new OcrSearchPageViewModel();

            Picker.ItemSource = ViewModel.SupportedLanguages;
        }
    }
}
