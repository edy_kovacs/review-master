﻿using System;
using Licenta.Presentation.ViewModels;
using Licenta.Presentation.ViewModels.Search;
using Xamarin.Forms;
using ZXing.Net.Mobile.Forms;

namespace Licenta.Presentation.Views.Search
{
    public class QrCodeSearchPage : ZXingScannerPage
    {
        public QrCodeSearchPageViewModel ViewModel
        {
            get => BindingContext as QrCodeSearchPageViewModel;
            set => BindingContext = value;
        }

        public QrCodeSearchPage()
        {
            ViewModel = new QrCodeSearchPageViewModel();
            SetupLayout();

            this.OnScanResult += async (result) =>
            {
                if (this.IsAnalyzing)
                {
                    await ViewModel.Handle_QrCodeScannedAsync(result.Text);
                }
            };

            this.SetBinding(IsScanningProperty, nameof(ViewModel.ShouldScanForQrCodes));
            this.SetBinding(IsAnalyzingProperty, nameof(ViewModel.ShouldScanForQrCodes));
        }

        private void SetupLayout()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, true);
            var layout = new AbsoluteLayout();
            AbsoluteLayout.SetLayoutFlags(this.Content, AbsoluteLayoutFlags.All);
            AbsoluteLayout.SetLayoutBounds(this.Content, new Rectangle(0, 0, 1, 1));

            var activityIndicator = new ActivityIndicator
            {
                HeightRequest = 40,
                WidthRequest = 40,
                Margin = new Thickness(0, 0, 0, 25),
                IsRunning = true,
                Color = Color.White
            };
            activityIndicator.SetBinding(ActivityIndicator.IsRunningProperty, nameof(ViewModel.IsBusy));
            activityIndicator.SetBinding(IsVisibleProperty, nameof(ViewModel.IsBusy));
            AbsoluteLayout.SetLayoutFlags(activityIndicator, AbsoluteLayoutFlags.PositionProportional);
            AbsoluteLayout.SetLayoutBounds(activityIndicator, new Rectangle(0.5, 1, 40, 65));

            layout.Children.Add(this.Content);
            layout.Children.Add(activityIndicator);
            layout.Children.Add(SetupBackButtonLayout());

            this.Content = layout;
        }

        private Button SetupBackButtonLayout()
        {
            var btn = new Button()
            {
                Image = "backarrow.png",
                Padding = 0,
                Margin = new Thickness(0),
                BackgroundColor = Color.Transparent,
            };

            btn.SetBinding(Button.CommandProperty, nameof(ViewModel.OnBackPressedCommand));
            AbsoluteLayout.SetLayoutFlags(btn, AbsoluteLayoutFlags.None);

            var layoutBounds = Device.iOS == Device.RuntimePlatform ? new Rectangle(0, 24, 50, 50) : new Rectangle(8, 8, 50, 50);
            AbsoluteLayout.SetLayoutBounds(btn, layoutBounds);

            return btn;
        }
    }
}

