﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading;
using Licenta.Presentation.ViewModels.Search;
using Xamarin.Forms;

namespace Licenta.Presentation.Views.Search
{
    public partial class SelectWordsSearchPage : BasePage
    {
        public SelectWordsSearchPageViewModel ViewModel
        {
            get => BindingContext as SelectWordsSearchPageViewModel;
            set => BindingContext = value;
        }

        public SelectWordsSearchPage(List<string> words)
        {
            InitializeComponent();

            ViewModel = new SelectWordsSearchPageViewModel(words);
        }

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(() => ViewModel.Navigation.PopToRootAsync());

            return true;
        }
    }
}
