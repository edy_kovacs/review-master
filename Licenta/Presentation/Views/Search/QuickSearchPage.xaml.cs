﻿using System;
using System.Collections.Generic;
using Licenta.Presentation.ViewModels.Search;
using Xamarin.Forms;

namespace Licenta.Presentation.Views.Search
{
    public partial class QuickSearchPage : BasePage
    {
        public QuickSearchPageViewModel ViewModel
        {
            get => BindingContext as QuickSearchPageViewModel;
            set => BindingContext = value;
        }

        public QuickSearchPage()
        {
            InitializeComponent();

            ViewModel = new QuickSearchPageViewModel();

            SetupGestureRecognizer();
        }

        private void SetupGestureRecognizer()
        {
            var tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.Tapped += Handle_ImageTapped;

            ContainerStackLayout.GestureRecognizers.Add(tapGestureRecognizer);
        }

        private void Handle_ImageTapped(object sender, EventArgs e)
        {
            ViewModel.TakePhotoAndGetOcrResultsCommand.Execute(null);
        }
    }
}
