﻿using Licenta.Presentation.ViewModels;
using Xamarin.Forms;

namespace Licenta.Presentation.Views
{
    public partial class BasePage : ContentPage
    {
        public BaseViewModel ViewModel
        {
            get => BindingContext as BaseViewModel;
            set => BindingContext = value;
        }

        public BasePage()
        {
            InitializeComponent();
        }
    }
}
