﻿using System.Collections.Generic;
using Licenta.Presentation.ViewModels;
using Licenta.Presentation.Views.CustomViews;
using Licenta.Utilities;
using Xamarin.Forms;

namespace Licenta.Presentation.Views.ProductDetails
{
    public partial class ProductDetailsPage : BasePage
    {
        public ProductDetailsPageViewModel ViewModel
        {
            get => BindingContext as ProductDetailsPageViewModel;
            set => BindingContext = value;
        }

        public ProductDetailsPage(ProductViewModel productViewModel)
        {
            InitializeComponent();

            ViewModel = new ProductDetailsPageViewModel();

            ViewModel.FetchProductDetails(productViewModel.Product.Id);

            MainStackLayout.Children.Add(GetConfiguredTabView());
        }

        private TabViewControl GetConfiguredTabView()
        {
            var tabView = new TabViewControl(new List<TabItem>()
            {
                new TabItem("Reviews", new ProductReviewsContentView()),
                new TabItem("Characteristics", new ProductCharacteristicsContentView()),
            });
            tabView.VerticalOptions = LayoutOptions.FillAndExpand;
            tabView.HeaderBackgroundColor = Color.White;
            tabView.HeaderTabTextColor = Constants.Colors.GreenishBlack;
            tabView.HeaderSelectionUnderlineColor = Constants.Colors.CustomTeal;
            tabView.HeaderSelectionUnderlineThickness = 1;
            tabView.HeaderLabelMargin = new Thickness(0, 0, 0, 4);

            return tabView;
        }
    }
}
