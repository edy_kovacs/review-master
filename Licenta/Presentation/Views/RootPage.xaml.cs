﻿using System;
using System.Collections.Generic;
using Licenta.Presentation.Views.CustomViews;
using Licenta.Presentation.Views.Search;
using Licenta.Utilities;
using Xamarin.Forms;

namespace Licenta.Presentation.Views
{
    public partial class RootPage : CustomTabbedPage
    {
        public RootPage()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);

            this.Children.Add(new NavigationPage(new RecentSearchesPage()) { Title = "Recent", Icon = "recentsearches.png", BarTextColor = Color.White });
            this.Children.Add(new NavigationPage(new QuickSearchPage()) { Title = "Quick Search", Icon = "quicksearch.png", BarTextColor = Color.White });
            this.Children.Add(new NavigationPage(new UserProfilePage()) { Title = "Settings", Icon = "settings.png", BarTextColor = Color.White });
        }
    }
}
