﻿using System;
using System.Collections.Generic;
using Licenta.Presentation.ViewModels;
using Xamarin.Forms;

namespace Licenta.Presentation.Views
{
    public partial class UserProfilePage : BasePage
    {
        public UserProfilePageViewModel ViewModel
        {
            get => BindingContext as UserProfilePageViewModel;
            set => BindingContext = value;
        }

        public UserProfilePage()
        {
            InitializeComponent();

            ViewModel = new UserProfilePageViewModel();

            SetupGestureRecognizers();
        }

        private void SetupGestureRecognizers()
        {
            var tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.Tapped += (s, e) =>
            {
                ViewModel.UploadPhotoCommand.Execute(null);
            };

            UserProfilePicture.GestureRecognizers.Add(tapGestureRecognizer);
            EmptyProfilePicture.GestureRecognizers.Add(tapGestureRecognizer);
        }
    }
}
