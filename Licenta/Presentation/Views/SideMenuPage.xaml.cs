﻿using Licenta.Presentation.ViewModels;

namespace Licenta.Presentation.Views
{
    public partial class SideMenuPage : BasePage
    {
        public SideMenuViewModel ViewModel
        {
            get => BindingContext as SideMenuViewModel;
            set => BindingContext = value;
        }

        public SideMenuPage()
        {
            InitializeComponent();

            ViewModel = new SideMenuViewModel();
        }
    }
}
