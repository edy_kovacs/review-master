﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Licenta.Presentation.Utilities;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace Licenta.Presentation.Views
{
    public partial class ChoosePhotoOriginPopup : PopupPage
    {
        TaskCompletionSource<UploadType> _taskCompletionSource;

        public ChoosePhotoOriginPopup()
        {
            InitializeComponent();
        }

        public async Task<UploadType> Show()
        {
            _taskCompletionSource = new TaskCompletionSource<UploadType>();
            await PopupNavigation.Instance.PushAsync(this);

            return await _taskCompletionSource.Task;
        }

        private async void Handle_TakePhotoClickedAsync(object sender, System.EventArgs e)
        {
            await DismissPopup();
            _taskCompletionSource.SetResult(UploadType.TakePicture);
        }

        private async void Handle_PickPhotoClickedAsync(object sender, System.EventArgs e)
        {
            await DismissPopup();
            _taskCompletionSource.SetResult(UploadType.PickPicture);
        }

        private async Task DismissPopup()
        {
            await PopupNavigation.Instance.PopAllAsync();
        }

        protected override bool OnBackgroundClicked()
        {
            return true;
        }
    }
}
