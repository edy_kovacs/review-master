﻿using Licenta.Presentation.ViewModels;

namespace Licenta.Presentation.Views
{
    public partial class LoginPage : BasePage
    {
        public LoginPageViewModel ViewModel
        {
            get => BindingContext as LoginPageViewModel;
            set => BindingContext = value;
        }

        public LoginPage()
        {
            InitializeComponent();

            ViewModel = new LoginPageViewModel();
        }
    }
}
