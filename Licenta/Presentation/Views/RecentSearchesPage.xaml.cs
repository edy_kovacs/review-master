﻿using Licenta.Presentation.ViewModels;
using Xamarin.Forms;

namespace Licenta.Presentation.Views
{
    public partial class RecentSearchesPage : BasePage
    {
        public RecentSearchesPageViewModel ViewModel
        {
            get => BindingContext as RecentSearchesPageViewModel;
            set => BindingContext = value;
        }

        public RecentSearchesPage()
        {
            InitializeComponent();

            ViewModel = new RecentSearchesPageViewModel();
        }

        void Handle_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null) { return; }

            ((ListView)sender).SelectedItem = null;
        }
    }
}
