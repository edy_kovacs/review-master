﻿using System;
using System.Collections.Generic;
using Licenta.Presentation.ViewModels;
using Xamarin.Forms;

namespace Licenta.Presentation.Views
{
    public partial class BaseWebViewPage : BasePage
    {
        public BaseWebViewPageViewModel ViewModel
        {
            get => BindingContext as BaseWebViewPageViewModel;
            set => BindingContext = value;
        }

        public BaseWebViewPage(string webViewURL, string navBarTitle)
        {
            InitializeComponent();

            ViewModel = new BaseWebViewPageViewModel();

            WebView.Source = webViewURL;
            CustomNavigationBar.Title = navBarTitle;
        }
    }
}
