﻿using System;
using Licenta.Utilities;
using Xamarin.Forms;

namespace Licenta.Presentation.Views.CustomViews
{
    public partial class CustomNavigationBar : StackLayout
    {
        public EventHandler LeftButtonClicked;
        public EventHandler RightButtonClicked;

        public static readonly BindableProperty TitleProperty = BindableProperty.Create(nameof(Title), typeof(string), typeof(CustomNavigationBar), string.Empty, propertyChanged: TitleChanged);
        public static readonly BindableProperty LeftButtonImageSourceProperty = BindableProperty.Create(nameof(LeftButtonImageSource), typeof(string), typeof(CustomNavigationBar), string.Empty, propertyChanged: LeftButtonImageSourceChanged);
        public static readonly BindableProperty LeftButtonCommandProperty = BindableProperty.Create(nameof(LeftButtonCommand), typeof(Command), typeof(CustomNavigationBar), null, propertyChanged: LeftButtonCommandChanged);
        public static readonly BindableProperty RightButtonImageSourceProperty = BindableProperty.Create(nameof(RightButtonImageSource), typeof(string), typeof(CustomNavigationBar), string.Empty, propertyChanged: RightButtonImageSourceChanged);
        public static readonly BindableProperty RightButtonCommandProperty = BindableProperty.Create(nameof(RightButtonCommand), typeof(Command), typeof(CustomNavigationBar), null, propertyChanged: RightButtonCommandChanged);
        public static readonly BindableProperty RightButtonVisibilityProperty = BindableProperty.Create(nameof(RightButtonVisibility), typeof(bool), typeof(CustomNavigationBar), true, propertyChanged: RightButtonVisibilityChanged);
        public static readonly BindableProperty RightButtonTextProperty = BindableProperty.Create(nameof(RightButtonText), typeof(string), typeof(CustomNavigationBar), string.Empty, propertyChanged: RightButtonTextChanged);
        public static readonly BindableProperty LeftButtonTextProperty = BindableProperty.Create(nameof(LeftButtonText), typeof(string), typeof(CustomNavigationBar), string.Empty, propertyChanged: LeftButtonTextChanged);
        public static readonly BindableProperty LeftButtonVisibilityProperty = BindableProperty.Create(nameof(LeftButtonVisibility), typeof(bool), typeof(CustomNavigationBar), true, propertyChanged: LeftButtonVisibilityChanged);

        public string Title
        {
            get => (string)GetValue(TitleProperty);
            set => SetValue(TitleProperty, value);
        }

        public string LeftButtonImageSource
        {
            get => (string)GetValue(LeftButtonImageSourceProperty);
            set => SetValue(LeftButtonImageSourceProperty, value);
        }

        public Command LeftButtonCommand
        {
            get => (Command)GetValue(LeftButtonCommandProperty);
            set => SetValue(LeftButtonCommandProperty, value);
        }

        public string RightButtonImageSource
        {
            get => (string)GetValue(RightButtonImageSourceProperty);
            set => SetValue(RightButtonImageSourceProperty, value);
        }

        public Command RightButtonCommand
        {
            get => (Command)GetValue(RightButtonCommandProperty);
            set => SetValue(RightButtonCommandProperty, value);
        }

        public bool RightButtonVisibility
        {
            get => (bool)GetValue(RightButtonVisibilityProperty);
            set => SetValue(RightButtonVisibilityProperty, value);
        }

        public bool LeftButtonVisibility
        {
            get => (bool)GetValue(LeftButtonVisibilityProperty);
            set => SetValue(LeftButtonVisibilityProperty, value);
        }

        public string RightButtonText
        {
            get => (string)GetValue(RightButtonVisibilityProperty);
            set => SetValue(RightButtonVisibilityProperty, value);
        }

        public string LeftButtonText
        {
            get => (string)GetValue(LeftButtonTextProperty);
            set => SetValue(LeftButtonTextProperty, value);
        }

        public CustomNavigationBar()
        {
            InitializeComponent();
        }

        static void RightButtonTextChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var view = bindable as CustomNavigationBar;

            if (view == null || newValue == null || Equals(oldValue, newValue)) { return; }

            view.RightButton.Text = newValue.ToString();
        }

        static void LeftButtonTextChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var view = bindable as CustomNavigationBar;

            if (view == null || newValue == null || Equals(oldValue, newValue)) { return; }

            view.LeftButton.Text = newValue.ToString();
        }

        static void TitleChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var view = bindable as CustomNavigationBar;

            if (view == null || newValue == null || Equals(oldValue, newValue)) { return; }

            view.TitleLabel.Text = newValue.ToString();
        }

        static void LeftButtonImageSourceChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var view = bindable as CustomNavigationBar;

            if (view == null || newValue == null || Equals(oldValue, newValue)) { return; }

            view.LeftButton.Image = newValue.ToString();
        }

        static void LeftButtonCommandChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var view = bindable as CustomNavigationBar;

            if (view == null || newValue == null || Equals(oldValue, newValue)) { return; }

            view.LeftButton.Command = (Command)newValue;
        }

        static void RightButtonImageSourceChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var view = bindable as CustomNavigationBar;

            if (view == null || newValue == null || Equals(oldValue, newValue)) { return; }

            view.RightButton.Image = newValue.ToString();
        }

        static void RightButtonCommandChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var view = bindable as CustomNavigationBar;

            if (view == null || newValue == null || Equals(oldValue, newValue)) { return; }

            view.RightButton.Command = (Command)newValue;
        }

        static void RightButtonVisibilityChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var view = bindable as CustomNavigationBar;

            if (view == null || newValue == null) { return; }

            view.RightButton.TextColor = (bool)newValue ? Color.White : Constants.Colors.WhiteLowOpacity;
        }

        static void LeftButtonVisibilityChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var view = bindable as CustomNavigationBar;

            if (view == null || newValue == null) { return; }

            view.LeftButton.TextColor = (bool)newValue ? Color.White : Constants.Colors.WhiteLowOpacity;
        }

        static void RightBuandttonTextChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var view = bindable as CustomNavigationBar;

            if (view == null || newValue == null || Equals(oldValue, newValue)) { return; }

            view.RightButton.Text = newValue.ToString();
        }

        void HandleLeftButtonClicked(object sender, EventArgs eventArgs)
        {
            if (sender == null) { return; }
            LeftButtonCommand.Execute(null);
            LeftButtonClicked.Invoke(this, null);
        }

        void HandleRightButtonClicked(object sender, EventArgs eventArgs)
        {
            if (sender == null) { return; }
            RightButtonCommand.Execute(null);
            RightButtonClicked.Invoke(this, null);
        }
    }
}
