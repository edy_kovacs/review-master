﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Licenta.Domain.Utilities;
using Licenta.Presentation.ViewModels.Search;
using Licenta.Utilities;
using Rg.Plugins.Popup.Enums;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;

namespace Licenta.Presentation.Views.CustomViews
{
    public partial class TitledPicker : ContentView
    {
        #region Private Fields

        private CustomPickerPopupPage _lastPopupPage;
        private readonly uint _defaultAnimationDuration = 250;
        private IList<(string title, string subtitle, object element)> _displayItems;

        #endregion

        #region Bindable Properties

        public static readonly BindableProperty PopupTitleProperty =
            BindableProperty.Create(nameof(PopupTitle), typeof(string), typeof(TitledPicker), string.Empty);

        public static readonly BindableProperty PlaceholderTextProperty =
            BindableProperty.Create(nameof(PlaceholderText), typeof(string), typeof(TitledPicker), string.Empty, propertyChanged: PlaceholderChanged);

        public static readonly BindableProperty ItemSourceProperty =
            BindableProperty.Create(nameof(ItemSource), typeof(IEnumerable), typeof(TitledPicker), null, propertyChanged: ItemSourceChanged);

        public static readonly BindableProperty SelectedItemProperty =
            BindableProperty.Create(nameof(SelectedItem), typeof(object), typeof(TitledPicker), null, propertyChanged: HandleSelectedItemChanged, defaultBindingMode: BindingMode.TwoWay);

        public static BindableProperty PropertyNameUsedForTitleProperty =
            BindableProperty.Create(nameof(PropertyNameUsedForTitle), typeof(string), typeof(TitledPicker));

        public static BindableProperty PropertyNameUsedForSubTitleProperty =
            BindableProperty.Create(nameof(PropertyNameUsedForSubTitle), typeof(string), typeof(TitledPicker));

        #endregion

        #region Properties

        public string PopupTitle
        {
            get { return GetValue(PopupTitleProperty) as string; }
            set { SetValue(PopupTitleProperty, value); }
        }

        public string PlaceholderText
        {
            get { return GetValue(PlaceholderTextProperty) as string; }
            set { SetValue(PlaceholderTextProperty, value); }
        }

        public IEnumerable ItemSource
        {
            get => (IEnumerable)GetValue(ItemSourceProperty);
            set => SetValue(ItemSourceProperty, value);
        }

        public object SelectedItem
        {
            get => GetValue(SelectedItemProperty);
            set => SetValue(SelectedItemProperty, value);
        }

        public string PropertyNameUsedForTitle
        {
            get { return GetValue(PropertyNameUsedForTitleProperty) as string; }
            set { SetValue(PropertyNameUsedForTitleProperty, value); }
        }

        public string PropertyNameUsedForSubTitle
        {
            get { return GetValue(PropertyNameUsedForSubTitleProperty) as string; }
            set { SetValue(PropertyNameUsedForSubTitleProperty, value); }
        }

        public event EventHandler<PickerEntryItemSelectedEventArgs> ItemSelected;
        public event EventHandler<EventArgs> ItemDeselected;

        #endregion

        public TitledPicker()
        {
            InitializeComponent();
            SetupView();
        }

        private void SetupView()
        {
            var tapGesture = new TapGestureRecognizer
            {
                NumberOfTapsRequired = 1
            };

            LabelTitle.Opacity = 0;
            tapGesture.Tapped += Handle_PickerTapped;
            this.GestureRecognizers.Add(tapGesture);
        }

        #region Bindable Property Changed Handlers

        private static void PlaceholderChanged(BindableObject bindable, object oldValue, object newValue)
        {
            // N.B. this is here to handle the first time binding
            bindable.ChangeBindableProperty<TitledPicker>(oldValue, newValue, (view) =>
            {
                if (view.SelectedItem == null)
                {
                    view.PickerLabel.Text = (string)newValue;
                    view.LabelTitle.Text = (string)newValue;
                    view.PickerLabel.TextColor = Constants.Colors.LightGrey;
                }
            });
        }

        private static void ItemSourceChanged(BindableObject bindable, object oldValue, object newValue)
        {
            bindable.ChangeBindableProperty<TitledPicker>(oldValue, newValue, (view) =>
            {
                view.SelectedItem = null;

                var newValues = (IEnumerable)newValue;

                if (view._displayItems == null)
                {
                    view._displayItems = new List<(string, string, object)>();
                }
                else
                {
                    view._displayItems.Clear();
                }

                foreach (var item in newValues)
                {
                    var title = item.GetPropertyValue(view.PropertyNameUsedForTitle).ToString();
                    string subtitle = null;
                    try
                    {
                        subtitle = item.GetPropertyValue(view.PropertyNameUsedForSubTitle).ToString();
                    }
                    catch
                    {
                        // subtitle not provided, do nothing
                    }

                    view._displayItems.Add((title, subtitle, item));
                }
            });
        }

        private static void HandleSelectedItemChanged(BindableObject bindable, object oldValue, object newValue)
        {
            bindable.ChangeBindableProperty<TitledPicker>(oldValue, newValue, (view) =>
            {
                if (view == null) { return; }
                var selectedObject = newValue;

                if (selectedObject == null)
                {
                    view.PickerLabel.Text = view.PlaceholderText;
                    view.PickerLabel.TextColor = Constants.Colors.LightGrey;
                    view.ItemDeselected?.Invoke(view, new EventArgs());
                }
                else
                {
                    view.PickerLabel.Text = selectedObject.GetPropertyValue(view.PropertyNameUsedForTitle).ToString(); ;
                    view.PickerLabel.TextColor = Color.Black;
                    view.ItemSelected?.Invoke(view, new PickerEntryItemSelectedEventArgs(selectedObject));
                }

            }, false, false);
            // N.B. null check is disabled because null is allowed in this case
        }

        #endregion

        #region Event Handlers

        void PickerPageElementSelected(object sender, PickerElementSelectedEventArgs args)
        {
            if (args.SelectedElement == null)
            {
                SelectedItem = null;
                LabelTitle.FadeTo(0, _defaultAnimationDuration);
            }
            else
            {
                var selectedPickerPopupElement = ((string title, string subtitle, object element))args.SelectedElement;
                SelectedItem = selectedPickerPopupElement.element;
            }

            PickerLabel.FadeTo(1, _defaultAnimationDuration);
            ArrowIcon.RotateTo(0, _defaultAnimationDuration).Forget();
            UnsubscribeFromEvents();
        }

        void PickerPageCancelClicked(object sender, EventArgs e)
        {
            if (SelectedItem == null)
            {
                PickerLabel.Text = PlaceholderText;
                PickerLabel.TextColor = Constants.Colors.LightGrey;
                LabelTitle.FadeTo(0, _defaultAnimationDuration);
                PickerLabel.FadeTo(1, _defaultAnimationDuration);
            }

            ArrowIcon.RotateTo(0, _defaultAnimationDuration).Forget();
            UnsubscribeFromEvents();
        }

        async void OnPickerTapped(object sender, EventArgs e)
        {
            if (ItemSource == null) { return; }
            var rotationDegree = (Math.Abs(ArrowIcon.Rotation) < double.Epsilon) ? -180 : 0;
            ArrowIcon.RotateTo(rotationDegree, _defaultAnimationDuration).Forget();

            var pickerPage = new CustomPickerPopupPage(PopupTitle)
            {
                SystemPaddingSides = Device.RuntimePlatform.Equals(Device.iOS) ? PaddingSide.Top | PaddingSide.Left | PaddingSide.Right : PaddingSide.All
            };
            _lastPopupPage = pickerPage;
            var pickerSelectedElement = _displayItems.FirstOrDefault(it => it.element.Equals(SelectedItem));
            pickerPage.SetElements(_displayItems, (it) => it.title, (it) => it.subtitle, pickerSelectedElement);

            pickerPage.ElementSelected += PickerPageElementSelected;
            pickerPage.CancelClicked += PickerPageCancelClicked;
            await Navigation.PushPopupAsync(pickerPage);
        }

        private void UnsubscribeFromEvents()
        {
            _lastPopupPage.ElementSelected -= PickerPageElementSelected;
            _lastPopupPage.CancelClicked -= PickerPageCancelClicked;
            _lastPopupPage = null;
        }

        #endregion

        async void Handle_PickerTapped(object sender, EventArgs e)
        {
            if (ItemSource == null) { return; }
            var rotationDegree = (Math.Abs(ArrowIcon.Rotation) < double.Epsilon) ? -180 : 0;
            ArrowIcon.RotateTo(rotationDegree, _defaultAnimationDuration).Forget();
            LabelTitle.FadeTo(1, _defaultAnimationDuration);

            if (SelectedItem == null)
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await PickerLabel.FadeTo(0, _defaultAnimationDuration);
                    PickerLabel.Text = "Default English";
                    PickerLabel.FadeTo(1, _defaultAnimationDuration);
                });
            }

            var pickerPage = new CustomPickerPopupPage(PopupTitle)
            {
                SystemPaddingSides = Device.RuntimePlatform.Equals(Device.iOS) ? PaddingSide.Top | PaddingSide.Left | PaddingSide.Right : PaddingSide.All
            };
            _lastPopupPage = pickerPage;
            var pickerSelectedElement = _displayItems.FirstOrDefault(it => it.element.Equals(SelectedItem));
            pickerPage.SetElements(_displayItems, (it) => it.title, (it) => it.subtitle, pickerSelectedElement);

            pickerPage.ElementSelected += PickerPageElementSelected;
            pickerPage.CancelClicked += PickerPageCancelClicked;
            await Navigation.PushPopupAsync(pickerPage);
        }

        public class PickerEntryItemSelectedEventArgs : EventArgs
        {
            public object Item { get; set; }

            public PickerEntryItemSelectedEventArgs(object item)
            {
                Item = item;
            }
        }
    }
}
