﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Licenta.Presentation.Views.CustomViews
{
    public partial class UserProfilePicture : ContentView
    {
        public static readonly BindableProperty SourceProperty = BindableProperty.Create(nameof(Source), typeof(string), typeof(UserProfilePicture), "noimage", propertyChanged: SourceChanged);

        public string Source
        {
            get => (string)GetValue(SourceProperty);
            set => SetValue(SourceProperty, value);
        }

        public UserProfilePicture()
        {
            InitializeComponent();
        }

        static void SourceChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var view = bindable as UserProfilePicture;

            if (view == null || newValue == null || Equals(oldValue, newValue)) { return; }

            view.ProfileImage.Source = newValue.ToString();
        }
    }
}
