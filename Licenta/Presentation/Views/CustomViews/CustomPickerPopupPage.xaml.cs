﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Licenta.Presentation.ViewModels;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using Xamarin.Forms;

namespace Licenta.Presentation.Views.CustomViews
{
    public class PickerElementSelectedEventArgs : EventArgs
    {
        public int SelectedIndex;
        public object SelectedElement;
        public string DisplayName;
    }

    public partial class CustomPickerPopupPage : PopupPage
    {
        private string _checkMarkBlueImage = "checkMarkBlue";
        private ObservableCollection<CustomPickerElement> _elements;
        private CustomPickerElement _currentlySelectedItem;

        public EventHandler<PickerElementSelectedEventArgs> ElementSelected;
        public event EventHandler CancelClicked;

        public CustomPickerPopupPage(string title)
        {
            InitializeComponent();

            TitleLabel.Text = title;
        }

        public void SetElements<T>(IEnumerable<T> elements, Func<T, string> title, Func<T, string> subtitle = null, object selectedElement = null)
        {
            _elements = new ObservableCollection<CustomPickerElement>(elements.Select((elm) => new CustomPickerElement() { Title = title(elm), Subtitle = subtitle?.Invoke(elm), Element = elm }));
            ElementsListView.ItemsSource = this._elements;

            var selectedItem = _elements.FirstOrDefault(elm => elm.Element.Equals(selectedElement));
            if (selectedItem != null)
            {
                selectedItem.ImageSource = _checkMarkBlueImage;
                ElementsListView.ScrollTo(selectedItem, Xamarin.Forms.ScrollToPosition.MakeVisible, animated: true);
            }
            _currentlySelectedItem = selectedItem;
        }

        #region Event Handlers 

        protected override bool OnBackgroundClicked()
        {
            return false;
        }

        void ListItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null) { return; }

            var isSameItemSelected = false;
            if (_currentlySelectedItem != null)
            {
                isSameItemSelected = _currentlySelectedItem.Equals(e.SelectedItem);
                // N.B. always reset/changhe the picture of the old element
                if (isSameItemSelected)
                {
                    _currentlySelectedItem.ImageSource = null;
                    _currentlySelectedItem = null;
                }
                else
                {
                    _currentlySelectedItem.ImageSource = null;
                    _currentlySelectedItem = (CustomPickerElement)e.SelectedItem;
                    _currentlySelectedItem.ImageSource = _checkMarkBlueImage;
                }
            }
            else
            {
                _currentlySelectedItem = (CustomPickerElement)e.SelectedItem;
                _currentlySelectedItem.ImageSource = _checkMarkBlueImage;
            }

            ElementsListView.SelectedItem = null;
            ChooseButton.IsEnabled = true;
        }

        void ChooseClicked(object sender, EventArgs e)
        {
            var selectedIndex = _currentlySelectedItem == null ? -1 : this._elements.IndexOf(_currentlySelectedItem);

            ElementSelected?.Invoke(this, new PickerElementSelectedEventArgs()
            {
                SelectedIndex = selectedIndex,
                SelectedElement = _currentlySelectedItem?.Element,
                DisplayName = _currentlySelectedItem?.Title
            });

            Navigation.PopPopupAsync();
        }

        void HandleCancelClicked(object sender, EventArgs e)
        {
            CancelClicked?.Invoke(this, e);
            this.Navigation.PopPopupAsync();
        }

        #endregion

        private class CustomPickerElement : BaseViewModel
        {
            public string Title { get; set; }
            public string Subtitle { get; set; }
            public string ImageSource { get; set; }
            public object Element { get; set; }
        }
    }
}
