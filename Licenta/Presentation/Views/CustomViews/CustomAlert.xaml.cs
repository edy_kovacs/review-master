﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Licenta.Utilities;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace Licenta.Presentation.Views.CustomViews
{
    public partial class CustomAlert : PopupPage
    {
        TaskCompletionSource<bool> _taskCompletionSource;

        public CustomAlert(string titleText, string subtitleText, string cancelButtonText, string confirmButtonText = null)
        {
            InitializeComponent();

            TitleLabel.Text = titleText;
            SubtitleLabel.Text = subtitleText;
            CancelButton.Text = cancelButtonText;

            if (confirmButtonText != null)
            {
                ConfirmButton.Text = confirmButtonText;
            }
            else
            {
                MakeLeftButtonFillTheSpace();
            }
        }

        public async Task<bool> ShowAsync()
        {
            if (_taskCompletionSource == null)
            {
                _taskCompletionSource = new TaskCompletionSource<bool>();
                await PopupNavigation.Instance.PushAsync(this);
            }

            return await _taskCompletionSource.Task;
        }

        private async void Handle_CancelButtonClickedAsync(object sender, System.EventArgs e)
        {
            _taskCompletionSource.TrySetResult(false);
            await DismissPopupAsync();
        }

        private async void Handle_ConfirmButtonClickedAsync(object sender, System.EventArgs e)
        {
            _taskCompletionSource.TrySetResult(true);
            await DismissPopupAsync();
        }

        private async Task DismissPopupAsync()
        {
            await PopupNavigation.Instance.PopAllAsync();
        }

        private void MakeLeftButtonFillTheSpace()
        {
            ButtonsSeparator.IsVisible = false;
            ConfirmButton.IsVisible = false;
            CancelButton.TextColor = Constants.Colors.PrimaryBlue;

            ButtonsGrid.ColumnDefinitions = new ColumnDefinitionCollection {
                new ColumnDefinition { Width = GridLength.Star },
                new ColumnDefinition { Width = new GridLength(0) },
                new ColumnDefinition { Width = new GridLength(0) },
            };
        }
    }
}
