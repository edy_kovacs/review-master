﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Licenta.Utilities;
using Xamarin.Forms;

namespace Licenta.Presentation.Views.CustomViews
{
    public partial class CustomListItemView : ContentView
    {
        #region Properties
        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public string Subtitle
        {
            get { return (string)GetValue(SubtitleProperty); }
            set { SetValue(SubtitleProperty, value); }
        }

        public ImageSource LeftImageSource
        {
            get { return (ImageSource)GetValue(LeftImageSourceProperty); }
            set { SetValue(LeftImageSourceProperty, value); }
        }

        public ImageSource RightImageSource
        {
            get { return (ImageSource)GetValue(RightImageSourceProperty); }
            set { SetValue(RightImageSourceProperty, value); }
        }

        public Color TitleTextColor
        {
            get { return (Color)GetValue(TitleTextColorProperty); }
            set { SetValue(TitleTextColorProperty, value); }
        }

        public double TitleFontSize
        {
            get { return (double)GetValue(TitleFontSizeProperty); }
            set { SetValue(TitleFontSizeProperty, value); }
        }

        public Color SubtitleTextColor
        {
            get { return (Color)GetValue(SubtitleTextColorProperty); }
            set { SetValue(SubtitleTextColorProperty, value); }
        }

        public double SubtitleFontSize
        {
            get { return (double)GetValue(SubtitleFontSizeProperty); }
            set { SetValue(SubtitleFontSizeProperty, value); }
        }

        public double LayoutSize
        {
            get { return (double)GetValue(LayoutSizeProperty); }
            set { SetValue(LayoutSizeProperty, value); }
        }

        public new Thickness Padding
        {
            get { return (Thickness)GetValue(PaddingProperty); }
            set { SetValue(PaddingProperty, value); }
        }

        public new Thickness Margin
        {
            get { return (Thickness)GetValue(MarginProperty); }
            set { SetValue(MarginProperty, value); }
        }

        public ObservableCollection<string> SmallImagesList
        {
            get { return (ObservableCollection<string>)GetValue(SmallImagesListProperty); }
            set { SetValue(SmallImagesListProperty, value); }
        }
        #endregion

        #region Bindable Properties
        public static readonly BindableProperty TitleProperty = BindableProperty.Create(
                                                                propertyName: nameof(Title),
                                                                returnType: typeof(string),
                                                                declaringType: typeof(CustomListItemView),
                                                                defaultValue: null,
                                                                propertyChanged: OnTitleChanged);

        public static readonly BindableProperty SubtitleProperty = BindableProperty.Create(
                                                                propertyName: nameof(Subtitle),
                                                                returnType: typeof(string),
                                                                declaringType: typeof(CustomListItemView),
                                                                defaultValue: null,
                                                                propertyChanged: OnSubtitleChanged);

        public static readonly BindableProperty LeftImageSourceProperty = BindableProperty.Create(
                                                                propertyName: nameof(LeftImageSource),
                                                                returnType: typeof(ImageSource),
                                                                declaringType: typeof(CustomListItemView),
                                                                defaultValue: null,
                                                                propertyChanged: OnLeftImageSourceChanged);

        public static readonly BindableProperty RightImageSourceProperty = BindableProperty.Create(
                                                                propertyName: nameof(RightImageSource),
                                                                returnType: typeof(ImageSource),
                                                                declaringType: typeof(CustomListItemView),
                                                                defaultValue: null,
                                                                propertyChanged: OnRightImageSourceChanged);

        public static readonly BindableProperty TitleTextColorProperty = BindableProperty.Create(
                                                                propertyName: nameof(TitleTextColor),
                                                                returnType: typeof(Color),
                                                                declaringType: typeof(CustomListItemView),
                                                                defaultValue: null,
                                                                propertyChanged: OnTitleTextColorChanged);

        public static readonly BindableProperty TitleFontSizeProperty = BindableProperty.Create(
                                                                propertyName: nameof(TitleFontSize),
                                                                returnType: typeof(double),
                                                                declaringType: typeof(CustomListItemView),
                                                                defaultValue: default(double),
                                                                propertyChanged: OnTitleFontSizeChanged);

        public static readonly BindableProperty SubtitleTextColorProperty = BindableProperty.Create(
                                                                 propertyName: nameof(SubtitleTextColor),
                                                                 returnType: typeof(Color),
                                                                 declaringType: typeof(CustomListItemView),
                                                                 defaultValue: null,
                                                                 propertyChanged: OnSubtitleTextColorChanged);

        public static readonly BindableProperty SubtitleFontSizeProperty = BindableProperty.Create(
                                                                propertyName: nameof(SubtitleFontSize),
                                                                returnType: typeof(double),
                                                                declaringType: typeof(CustomListItemView),
                                                                defaultValue: default(double),
                                                                propertyChanged: OnSubtitleFontSizeChanged);

        public static readonly BindableProperty LayoutSizeProperty = BindableProperty.Create(
                                                                 propertyName: nameof(LayoutSize),
                                                                 returnType: typeof(double),
                                                                 declaringType: typeof(CustomListItemView),
                                                                 defaultValue: default(double),
                                                                 propertyChanged: OnLayoutSizeChanged);

        public static new readonly BindableProperty PaddingProperty = BindableProperty.Create(
                                                                propertyName: nameof(Padding),
                                                                returnType: typeof(Thickness),
                                                                declaringType: typeof(CustomListItemView),
                                                                defaultValue: null,
                                                                propertyChanged: OnPaddingChanged);

        public static new readonly BindableProperty MarginProperty = BindableProperty.Create(
                                                                propertyName: nameof(Margin),
                                                                returnType: typeof(Thickness),
                                                                declaringType: typeof(CustomListItemView),
                                                                defaultValue: null,
                                                                propertyChanged: OnMarginChanged);

        public static readonly BindableProperty SmallImagesListProperty = BindableProperty.Create(
                                                                propertyName: nameof(SmallImagesList),
                                                                returnType: typeof(ObservableCollection<string>),
                                                                declaringType: typeof(CustomListItemView),
                                                                defaultValue: null,
                                                                propertyChanged: OnSmallImagesChanged);

        #endregion

        #region OnPropertyChanged handlers

        private static void OnTitleChanged(BindableObject bindable, object oldValue, object newValue)
        {
            bindable.ChangeBindableProperty<CustomListItemView>(oldValue, newValue, (view) =>
            {
                view.TitleLabel.Text = (string)newValue;
            });
        }

        private static void OnSubtitleChanged(BindableObject bindable, object oldValue, object newValue)
        {
            bindable.ChangeBindableProperty<CustomListItemView>(oldValue, newValue, (view) =>
            {
                var newText = (string)newValue;
                view.SubtitleLabel.IsVisible = !string.IsNullOrWhiteSpace(newText);
                view.SubtitleLabel.Text = (string)newValue;
            });
        }

        private static void OnLeftImageSourceChanged(BindableObject bindable, object oldValue, object newValue)
        {
            bindable.ChangeBindableProperty<CustomListItemView>(oldValue, newValue, nullCheck: false, propertyChangedHandler: (view) =>
            {
                var hasValue = newValue != null;
                view.LeftImage.IsVisible = hasValue;
                view.LeftImage.Source = (ImageSource)newValue;
            });
        }

        private static void OnRightImageSourceChanged(BindableObject bindable, object oldValue, object newValue)
        {
            bindable.ChangeBindableProperty<CustomListItemView>(oldValue, newValue, nullCheck: false, propertyChangedHandler: (view) =>
            {
                var hasValue = newValue != null;
                view.RightImage.IsVisible = hasValue;
                view.RightImage.Source = (ImageSource)newValue;
            });
        }

        private static void OnTitleTextColorChanged(BindableObject bindable, object oldValue, object newValue)
        {
            bindable.ChangeBindableProperty<CustomListItemView>(oldValue, newValue, (view) =>
            {
                view.TitleLabel.TextColor = (Color)newValue;
            });
        }

        private static void OnTitleFontSizeChanged(BindableObject bindable, object oldValue, object newValue)
        {
            bindable.ChangeBindableProperty<CustomListItemView>(oldValue, newValue, (view) =>
            {
                view.TitleLabel.FontSize = (double)newValue;
            });
        }

        private static void OnSubtitleTextColorChanged(BindableObject bindable, object oldValue, object newValue)
        {
            bindable.ChangeBindableProperty<CustomListItemView>(oldValue, newValue, (view) =>
            {
                view.SubtitleLabel.TextColor = (Color)newValue;
            });
        }

        private static void OnSubtitleFontSizeChanged(BindableObject bindable, object oldValue, object newValue)
        {
            bindable.ChangeBindableProperty<CustomListItemView>(oldValue, newValue, (view) =>
            {
                view.SubtitleLabel.FontSize = (double)newValue;
            });
        }

        private static void OnPaddingChanged(BindableObject bindable, object oldValue, object newValue)
        {
            bindable.ChangeBindableProperty<CustomListItemView>(oldValue, newValue, (view) =>
            {
                view.MainGrid.Padding = (Thickness)newValue;
            });
        }

        private static void OnLayoutSizeChanged(BindableObject bindable, object oldValue, object newValue)
        {
            bindable.ChangeBindableProperty<CustomListItemView>(oldValue, newValue, (view) =>
            {
                var newSize = (double)newValue;
                const int additionalLayoutSpace = 8;
                view.MainGrid.HeightRequest = newSize + additionalLayoutSpace;
                view.LeftImage.HeightRequest = newSize;
                view.LeftImage.WidthRequest = newSize;
            });
        }

        private static void OnMarginChanged(BindableObject bindable, object oldValue, object newValue)
        {
            bindable.ChangeBindableProperty<CustomListItemView>(oldValue, newValue, (view) =>
            {
                view.MainGrid.Margin = (Thickness)newValue;
            });
        }

        private static void OnSmallImagesChanged(BindableObject bindable, object oldValue, object newValue)
        {
            bindable.ChangeBindableProperty<CustomListItemView>(oldValue, newValue, (view) =>
            {
                var collection = (ObservableCollection<string>)newValue;
                BindableLayout.SetItemsSource(view.SmallImagesLayout, collection);
            });
        }

        #endregion

        public CustomListItemView()
        {
            InitializeComponent();
        }
    }
}
