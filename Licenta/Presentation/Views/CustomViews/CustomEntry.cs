﻿using Xamarin.Forms;

namespace Licenta.Presentation.Views.CustomViews
{
    public class CustomEntry : Entry
    {
        public static readonly BindableProperty HasBorderProperty = BindableProperty.Create(nameof(HasBorder), typeof(bool), typeof(CustomEntry), true);

        public bool HasBorder
        {
            get { return (bool)GetValue(HasBorderProperty); }
            set { SetValue(HasBorderProperty, value); }
        }
    }
}
