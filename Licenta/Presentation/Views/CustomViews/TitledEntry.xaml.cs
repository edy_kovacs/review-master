﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Licenta.Presentation.Views.CustomViews
{
    public partial class TitledEntry : ContentView
    {
        private string _lastTitle;
        private uint _animationDuration = 200;

        public static BindableProperty PlaceholderProperty = BindableProperty.Create(nameof(Placeholder), typeof(string), typeof(TitledEntry), String.Empty);
        public string Placeholder
        {
            get => (string)GetValue(PlaceholderProperty);
            set => SetValue(PlaceholderProperty, value);
        }

        public static BindableProperty TitleProperty = BindableProperty.Create(nameof(Title), typeof(string), typeof(TitledEntry), null, BindingMode.TwoWay);
        public string Title
        {
            get => (string)GetValue(TitleProperty);
            set => SetValue(TitleProperty, value);
        }

        public static BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(string), typeof(TitledEntry), String.Empty, propertyChanged: TextChanged);
        public string Text
        {
            get => (string)base.GetValue(TextProperty);
            set => base.SetValue(TextProperty, value);
        }

        public static BindableProperty KeyboardProperty = BindableProperty.Create(nameof(Keyboard), typeof(Keyboard), typeof(TitledEntry), Keyboard.Numeric, propertyChanged: KeyboardChanged);
        public Keyboard Keyboard
        {
            get => (Keyboard)base.GetValue(TextProperty);
            set => base.SetValue(TextProperty, KeyboardProperty);
        }

        public static BindableProperty IsPasswordProperty = BindableProperty.Create(nameof(IsPassword), typeof(bool), typeof(TitledEntry), false, propertyChanged: IsPasswordChanged);
        public bool IsPassword
        {
            get => (bool)base.GetValue(IsPasswordProperty);
            set => base.SetValue(IsPasswordProperty, value);
        }

        static void KeyboardChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var view = bindable as TitledEntry;

            if (view == null || newValue == null || Equals(oldValue, newValue)) { return; }

            view.EntryContent.Keyboard = (Keyboard)newValue;
        }

        static void IsPasswordChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var view = bindable as TitledEntry;

            if (view == null || newValue == null || Equals(oldValue, newValue)) { return; }

            view.EntryContent.IsPassword = (bool)newValue;
        }

        static void TextChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var view = bindable as TitledEntry;

            if (view == null || newValue == null || Equals(oldValue, newValue)) { return; }

            view.EntryContent.Text = newValue.ToString();

            if (view.EntryContent.Text == String.Empty || view.EntryContent.Text == null)
            {
                if(!view.EntryContent.IsFocused)
                {
                    view.LabelTitle.Opacity = 0;
                }
            }
            else
            {
                view.LabelTitle.Opacity = 1;
            }
        }

        public Thickness LabelMargin => Device.RuntimePlatform == Device.iOS ? new Thickness(0, 0, 0, 2) : new Thickness(0, 0, 0, -8);
        public Thickness EntryMargin => Device.RuntimePlatform == Device.iOS ? new Thickness(0, 1, 0, 5) : new Thickness(-3, 0, 0, -8);

        public TitledEntry()
        {
            InitializeComponent();

            EntryContent.BindingContext = this;
            LabelTitle.BindingContext = this;

            LabelTitle.Opacity = 0;
        }

        void Handle_Focused(object sender, FocusEventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                LabelTitle.FadeTo(1, _animationDuration);
                _lastTitle = EntryContent.Placeholder;
                EntryContent.Placeholder = Placeholder;
                if(EntryContent.Equals(String.Empty))
                {
                    await EntryContent.FadeTo(1, _animationDuration);
                }
            });
        }

        void Handle_Unfocused(object sender, FocusEventArgs e)
        {
            EntryContent.FadeTo(0, 1);
            EntryContent.Placeholder = _lastTitle;
            EntryContent.FadeTo(1, _animationDuration);

            if (EntryContent.Text == String.Empty || EntryContent.Text == null)
            {
               LabelTitle.FadeTo(0, _animationDuration);
            }
        }
    }
}
