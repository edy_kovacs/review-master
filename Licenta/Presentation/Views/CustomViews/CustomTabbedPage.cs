﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;

namespace Licenta.Presentation.Views.CustomViews
{
    public class CustomTabbedPage : Xamarin.Forms.TabbedPage
    {
        public CustomTabbedPage() : base()
        {
            On<Xamarin.Forms.PlatformConfiguration.Android>().SetToolbarPlacement(ToolbarPlacement.Bottom);
        }
    }
}

