﻿using System;
using System.Windows.Input;
using Licenta.Utilities;
using Xamarin.Forms;

namespace Licenta.Presentation.Views.CustomViews
{
    public class FrameWithTouchFeedback : Frame
    {
        public static readonly BindableProperty CommandProperty = BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(FrameWithTouchFeedback));
        public static readonly BindableProperty CommandParameterProperty = BindableProperty.Create(nameof(CommandParameter), typeof(object), typeof(FrameWithTouchFeedback));
        public static readonly BindableProperty PreferredBackgroundColorProperty = BindableProperty.Create(nameof(PreferredBackgroundColor), typeof(Color), typeof(FrameWithTouchFeedback), Color.Transparent, propertyChanged: OnPreferredBackgroundColorChanged);
        public static readonly BindableProperty PreferredHighlightColorProperty = BindableProperty.Create(nameof(PreferredHighlightColor), typeof(Color), typeof(FrameWithTouchFeedback), Constants.Colors.CustomTeal);

        public Color PreferredBackgroundColor
        {
            get { return (Color)GetValue(PreferredBackgroundColorProperty); }
            set { SetValue(PreferredBackgroundColorProperty, value); }
        }

        public Color PreferredHighlightColor
        {
            get { return (Color)GetValue(PreferredHighlightColorProperty); }
            set { SetValue(PreferredHighlightColorProperty, value); }
        }

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public object CommandParameter
        {
            get { return GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        private static void OnPreferredBackgroundColorChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var view = bindable as FrameWithTouchFeedback;

            if (view == null || newValue == null || Equals(oldValue, newValue)) { return; }

            view.BackgroundColor = (Color)newValue;
        }

        private ICommand TransitionCommand
        {
            get
            {
                return new Command(() =>
                {
                    //could add custom animation here if needed
                    if (Command != null)
                    {
                        Command.Execute(CommandParameter);
                    }
                });
            }
        }

        public FrameWithTouchFeedback()
        {
            BackgroundColor = PreferredBackgroundColor;
            GestureRecognizers.Add(new TapGestureRecognizer()
            {
                Command = TransitionCommand
            });
        }
    }
}
