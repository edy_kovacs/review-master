﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Licenta.Presentation.Views.CustomViews
{
    public partial class EmptyProfilePicture : ContentView
    {
        public static readonly BindableProperty InsideLabelTextProperty = BindableProperty.Create(nameof(InsideLabelText), typeof(string), typeof(EmptyProfilePicture), string.Empty, propertyChanged: InsideLabelTextChanged);

        public string InsideLabelText
        {
            get => (string)GetValue(InsideLabelTextProperty);
            set => SetValue(InsideLabelTextProperty, value);
        }

        public EmptyProfilePicture()
        {
            InitializeComponent();
        }

        static void InsideLabelTextChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var view = bindable as EmptyProfilePicture;

            if (view == null || newValue == null || Equals(oldValue, newValue)) { return; }

            view.InsideLabel.Text = newValue.ToString();
        }
    }
}
