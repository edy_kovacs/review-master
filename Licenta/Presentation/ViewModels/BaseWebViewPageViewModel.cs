﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Licenta.Presentation.ViewModels
{
    public class BaseWebViewPageViewModel : BaseViewModel
    {
        public ICommand OnBackPressedCommand { get; private set; }

        public BaseWebViewPageViewModel()
        {
            OnBackPressedCommand = new Command(async () => await OnBackPressedAsync());
        }

        private async Task OnBackPressedAsync()
        {
            await Navigation.PopAsync();
        }
    }
}
