﻿using System;
using Licenta.Utilities;
using Xamarin.Forms;

namespace Licenta.Presentation.ViewModels
{
    public class WordViewModel : BaseViewModel
    {
        public string Text { get; set; } = String.Empty;
        public bool IsSelected { get; set; }

        public Color FrameBackgroundColor => IsSelected ? Constants.Colors.CustomTeal : Color.White;
        public Color LabelTextColor => IsSelected ? Color.White : Color.Black;
        public Color FrameBorderColor => IsSelected ? Color.White : Constants.Colors.LighterGrey;

        public WordViewModel(string word)
        {
            Text = word;
        }
    }
}
