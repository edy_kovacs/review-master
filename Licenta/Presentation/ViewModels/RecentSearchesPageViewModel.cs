﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Licenta.Data.Repository;
using Licenta.Domain.Models;
using Licenta.Domain.Services;
using Licenta.Domain.Utilities;
using Licenta.Presentation.ViewModels.Search;
using Licenta.Presentation.Views;
using Licenta.Presentation.Views.ProductDetails;
using Licenta.Utilities;
using Newtonsoft.Json;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Licenta.Presentation.ViewModels
{
    public class RecentSearchesPageViewModel : BaseViewModel
    {
        public ICommand OpenSideMenuCommand { get; set; }
        public ICommand BeginNewSearchCommand { get; set; }

        public ObservableCollection<ProductViewModel> RecentSearchesList { get; set; } = new ObservableCollection<ProductViewModel>(new List<ProductViewModel>());
        public bool ShouldDisplayEmptyState { get; set; }

        private Command _productSelectedCommand;
        public Command ProductSelectedCommand
        {
            get => _productSelectedCommand ?? (_productSelectedCommand = new Command(async (productViewModel) => await OnProductSelected((ProductViewModel)productViewModel)));
        }

        private RecentSearchesService _recentSearchesService;
        private RecentSearchesSubscriber _recentSearchesSubscriber;

        public RecentSearchesPageViewModel()
        {
            OpenSideMenuCommand = new Command(() => OpenSideMenu());
            BeginNewSearchCommand = new Command(execute: async () => await BeginNewSearchSearchAsync());

            _recentSearchesSubscriber = new RecentSearchesSubscriber(this);
            _recentSearchesService = new RecentSearchesService(new RecentSearchesApiRepository());

            MessagingCenter.Subscribe<Application, ProductViewModel>(this, "ProductSelected", (sender, arg) => PlaceAsFirstInList((ProductViewModel)arg));

            FetchRecentSearches();
        }

        public void FetchRecentSearches()
        {
            var authToken = JsonConvert.DeserializeObject(Preferences.Get(Constants.RestStrings.Token, String.Empty)).ToString();
            _recentSearchesService.FetchRecentSearches(_recentSearchesSubscriber, authToken);
            IsBusy = true;
        }

        private void OpenSideMenu()
        {
            var navigationStack = Application.Current.MainPage.Navigation.NavigationStack;
            var rootPage = (navigationStack[0] as MasterDetailPage);
            rootPage.IsPresented = true;
        }

        private async Task BeginNewSearchSearchAsync()
        {
            await Navigation.PushAsync(new ChooseSearchTypePage());
        }

        private async Task OnProductSelected(ProductViewModel productViewModel)
        {
            if (productViewModel == null) return;

            await Navigation.PushAsync(new Views.ProductDetails.ProductDetailsPage(productViewModel));

            PlaceAsFirstInList(productViewModel);
        }

        private void PlaceAsFirstInList(ProductViewModel productViewModel)
        {
            if (RecentSearchesList.Contains(productViewModel))
            {
                RecentSearchesList.Remove(RecentSearchesList.Where(i => i.Product.Id == productViewModel.Product.Id).Single());
            }

            RecentSearchesList.Insert(0, productViewModel);
            ShouldDisplayEmptyState = false;
        }

        private class RecentSearchesSubscriber : IObserver<List<ProductDomainModel>>
        {
            private RecentSearchesPageViewModel _recentSearchesPageViewModel;

            public RecentSearchesSubscriber(RecentSearchesPageViewModel viewModel)
            {
                _recentSearchesPageViewModel = viewModel;
            }

            public void OnCompleted()
            {
                Logger.Instance.Info("Licenta. Recent Searches Request Complete.");
                _recentSearchesPageViewModel.IsBusy = false;
            }

            public void OnError(Exception error)
            {
                Logger.Instance.Error("Licenta. Recent Searches Request Error.");
                Logger.Instance.Error(error.Message);
                _recentSearchesPageViewModel.IsBusy = false;
            }

            public void OnNext(List<ProductDomainModel> value)
            {
                Logger.Instance.Info("Licenta. Recent Searches Response.");

                // Some strange bug if this is removed
                Device.BeginInvokeOnMainThread(() =>
                {
                    var recentSearchesList = value.Select(productDomainModel => new ProductViewModel(productDomainModel)).ToList();
                    recentSearchesList.ForEach(recentSearch => _recentSearchesPageViewModel.RecentSearchesList.Add(recentSearch));
                    _recentSearchesPageViewModel.ShouldDisplayEmptyState = recentSearchesList.Count == 0;
                });
            }
        }
    }
}
