﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Input;
using Licenta.Data.Converters;
using Licenta.Data.Models;
using Licenta.Data.Repository;
using Licenta.Domain.Models;
using Licenta.Domain.Services;
using Licenta.Domain.Utilities;
using Licenta.Presentation.Views;
using Licenta.Utilities;
using Newtonsoft.Json;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Licenta.Presentation.ViewModels
{
    public class SideMenuViewModel : BaseViewModel
    {
        public ICommand LogoutCommand { get; private set; }
        public ICommand UploadPhotoCommand { get; private set; }

        public UserViewModel UserViewModel { get; set; }
        public bool ShouldDisplayUploadPhotoButton
        {
            get
            {
                if (UserViewModel != null)
                {
                    return UserViewModel.User.PhotoURL == null;
                }

                return false;
            }
        }

        private AuthenticationService _authenticationService;
        private LogoutSubscriber _logoutSubscriber;

        public SideMenuViewModel()
        {
            _authenticationService = new AuthenticationService(new AuthenticationApiRepository());
            _logoutSubscriber = new LogoutSubscriber(this);

            LogoutCommand = new Command(() => Logout());
            UploadPhotoCommand = new Command(async () => await HandleUploadPhotoAsync());

            UserViewModel = JsonConvert.DeserializeObject<UserViewModel>(Preferences.Get(Constants.RestStrings.LoggedInUser, String.Empty));
        }

        private async Task MakeRequestWithPhoto(MediaFile _image)
        {
            if (_image == null) return;

            IsBusy = true;

            var content = new MultipartFormDataContent();
            content.Add(new StreamContent(_image.GetStream()), "\"file\"", $"\"{_image.Path}\"");

            var httpClient = new System.Net.Http.HttpClient();

            var authToken = JsonConvert.DeserializeObject(Preferences.Get(Constants.RestStrings.Token, String.Empty)).ToString();
            httpClient.DefaultRequestHeaders.Add("x-auth", authToken);

            var url = "http://nodetesseract.tk/api/coverImage";
            var responseMsg = await httpClient.PostAsync(url, content);

            if (responseMsg.IsSuccessStatusCode)
            {
                var responseContent = await responseMsg.Content.ReadAsStringAsync();
                UserDataModel receivedUser = (UserDataModel)JsonConvert.DeserializeObject<UserDataModel>(responseContent);
                var userDomainModel = new UserModelConverter().Transform(receivedUser);
                var userViewModel = new UserViewModel()
                {
                    User = userDomainModel
                };

                UserViewModel = userViewModel;
                Preferences.Set(Constants.RestStrings.LoggedInUser, JsonConvert.SerializeObject(userViewModel));
            }
            else
            {
                await Alert.ShowAsync("We are very sorry..", "Something unnexpected happened. Please try again later", "Ok");
            }

            IsBusy = false;
        }

        private async Task HandleUploadPhotoAsync()
        {
            var popup = new ChoosePhotoOriginPopup();
            var result = await popup.Show();

            if (result == Utilities.UploadType.PickPicture)
            {
                if (CrossMedia.Current.IsPickPhotoSupported)
                {
                    var photo = await CrossMedia.Current.PickPhotoAsync();
                    await MakeRequestWithPhoto(photo);
                }
            }
            else if (result == Utilities.UploadType.TakePicture)
            {
                var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

                if (cameraStatus != PermissionStatus.Granted)
                {
                    var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Camera);
                    cameraStatus = results[Permission.Camera];
                }

                if (cameraStatus == PermissionStatus.Granted && storageStatus == PermissionStatus.Granted)
                {
                    var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                    {
                        Directory = "Images",
                        Name = "test.jpeg"
                    });

                    await MakeRequestWithPhoto(file);
                }
                else
                {
                    await Alert.ShowAsync("Permissions Denied", "Unable to take photos.", "OK");
                    CrossPermissions.Current.OpenAppSettings();
                }
            }
            else
            {
                return;
            }
        }

        private void Logout()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                var action = await Alert.ShowAsync(Constants.Strings.AreYouSureText, Constants.Strings.LogoutText, Constants.Strings.NoText, Constants.Strings.YesText);

                if (action)
                {
                    IsBusy = true;

                    var authToken = JsonConvert.DeserializeObject(Preferences.Get(Constants.RestStrings.Token, String.Empty)).ToString();
                    _authenticationService.Logout(_logoutSubscriber, authToken);
                }
            });
        }

        private class LogoutSubscriber : IObserver<object>
        {
            private SideMenuViewModel _sideMenuViewModel;

            public LogoutSubscriber(SideMenuViewModel viewModel)
            {
                _sideMenuViewModel = viewModel;
            }

            public void OnCompleted()
            {
                Logger.Instance.Info("DeviceManager. Logout Request Complete.");

                _sideMenuViewModel.IsBusy = false;
            }

            public void OnError(Exception error)
            {
                Logger.Instance.Error("DeviceManager. Logout Request Error.");
                _sideMenuViewModel.IsBusy = false;
            }

            public void OnNext(object value)
            {
                Logger.Instance.Info("DeviceManager. Logout Response.");

                Preferences.Set(Constants.RestStrings.Token, string.Empty);
                Preferences.Set(Constants.RestStrings.LoggedInUser, string.Empty);

                Device.BeginInvokeOnMainThread(() => _sideMenuViewModel.Navigation.ReplaceRootPage(new NavigationPage(new LoginPage())));
            }
        }
    }
}
