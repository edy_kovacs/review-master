﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Input;
using Licenta.Data.Converters;
using Licenta.Data.Models;
using Licenta.Data.Repository;
using Licenta.Domain.Models;
using Licenta.Domain.Services;
using Licenta.Domain.Utilities;
using Licenta.Presentation.Utilities;
using Licenta.Presentation.Views;
using Licenta.Utilities;
using Newtonsoft.Json;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Licenta.Presentation.ViewModels
{
    public class UserProfilePageViewModel : BaseViewModel
    {
        public ICommand LogoutCommand { get; private set; }
        public ICommand UploadPhotoCommand { get; private set; }
        public ICommand SaveChangesCommand { get; private set; }
        public ICommand ResetChangesCommand { get; private set; }
        public ICommand OpenTermsPageCommand { get; private set; }
        public ICommand OpenAboutPageCommand { get; private set; }

        public UserViewModel UserViewModel { get; set; }
        public bool IsInfoDirty { get; set; }
        public bool ShouldDisplayEmptyProfilePicture => UserViewModel?.User?.PhotoURL == null;
        public string DisplayName { get; set; }
        public string Email { get; set; }

        private AuthenticationService _authenticationService;
        private LogoutSubscriber _logoutSubscriber;
        private UpdateProfileSubscriber _updateProfileSubscriber;

        public Thickness AboutButtonMargin => Device.RuntimePlatform == Device.iOS ? new Thickness(-8, 0, 0, 0) : new Thickness(4, 0, 0, 0);
        public Thickness TermsButtonMargin => Device.RuntimePlatform == Device.iOS ? new Thickness(-8, 0, 0, 0) : new Thickness(4, 0, 0, 0);
        public Thickness LogoutButtonMargin => Device.RuntimePlatform == Device.iOS ? new Thickness(-6, 0, 0, 0) : new Thickness(4, 0, 0, 0);
        public string EditText => Device.RuntimePlatform == Device.iOS ? "   EDIT" : "    EDIT";

        public UserProfilePageViewModel()
        {
            _authenticationService = new AuthenticationService(new AuthenticationApiRepository());
            _logoutSubscriber = new LogoutSubscriber(this);
            _updateProfileSubscriber = new UpdateProfileSubscriber(this);

            LogoutCommand = new Command(() => Logout());
            UploadPhotoCommand = new Command(async () => await HandleUploadPhotoAsync());
            SaveChangesCommand = new Command(async () => await SaveChangesAsync());
            ResetChangesCommand = new Command(async () => await ResetChangesAsync());
            OpenTermsPageCommand = new Command(async () => await OpenExtraPageAsync("http://nodetesseract.tk/api/terms", "Terms and Conditions"));
            OpenAboutPageCommand = new Command(async () => await OpenExtraPageAsync("http://nodetesseract.tk/api/privacy", "Privacy Policy"));

            UserViewModel = JsonConvert.DeserializeObject<UserViewModel>(Preferences.Get(Constants.RestStrings.LoggedInUser, String.Empty));

            SetFieldsToDefaultValues();
        }

        private async Task OpenExtraPageAsync(string url, string title)
        {
            await Navigation.PushAsync(new BaseWebViewPage(url, title));
        }

        private async Task ResetChangesAsync()
        {
            if(IsInfoDirty)
            {
                var res = await Alert.ShowAsync("Are you sure?", "This is going to reset the edits you have made", "No", "Yes");

                if(res)
                {
                    SetFieldsToDefaultValues();
                }
            }
        }

        private void SetFieldsToDefaultValues()
        {
            DisplayName = UserViewModel.User.Name;
            Email = UserViewModel.User.Email;
        }

        private async Task SaveChangesAsync()
        {
            if(IsInfoDirty)
            {
                IsBusy = true;

                try
                {
                    ValidateInputs();

                    var authToken = JsonConvert.DeserializeObject(Preferences.Get(Constants.RestStrings.Token, String.Empty)).ToString();
                    var credentials = new CredentialsDomainModel
                    {
                        Email = Email,
                        DisplayName = DisplayName,
                        Password = String.Empty
                    };

                    _authenticationService.UpdateProfile(_updateProfileSubscriber, credentials, authToken);
                }
                catch (InputsValidationException ex)
                {
                    await Alert.ShowAsync("Validation Error", ex.Message, "Ok");
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }

        private void ValidateInputs()
        {
            if (Email.IsNullOrEmpty())
            {
                throw new InputsValidationException("Email field cannot be empty");
            }

            if(DisplayName.IsNullOrEmpty())
            {
                throw new InputsValidationException("Display Name field cannot be empty");
            }

            if(!OtherMethods.IsValidEmail(Email))
            {
                throw new InputsValidationException("Please fill in a valid email address");
            }

            if(DisplayName.Length < 4)
            {
                throw new InputsValidationException("Display Name should be at least 3 characters long");
            }
        }

        public void OnDisplayNameChanged()
        {
            IsInfoDirty = !DisplayName.Equals(UserViewModel?.User?.Name);
        }

        public void OnEmailChanged()
        {
            IsInfoDirty = !Email.Equals(UserViewModel?.User?.Email);
        }

        private async Task HandleUploadPhotoAsync()
        {
            var popup = new ChoosePhotoOriginPopup();
            var result = await popup.Show();

            if (result == Utilities.UploadType.PickPicture)
            {
                if (CrossMedia.Current.IsPickPhotoSupported)
                {
                    var photo = await OtherMethods.PickPhotoAsync(Alert);
                    await MakeRequestWithPhoto(photo);
                }
            }
            else if (result == Utilities.UploadType.TakePicture)
            {
                if(CrossMedia.Current.IsTakePhotoSupported)
                {
                    var photo = await OtherMethods.TakePhotoAsync(Alert);
                    await MakeRequestWithPhoto(photo);
                }
            }
            else
            {
                return;
            }
        }

        private void Logout()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                var action = await Alert.ShowAsync(Constants.Strings.AreYouSureText, Constants.Strings.LogoutText, Constants.Strings.NoText, Constants.Strings.YesText);

                if (action)
                {
                    IsBusy = true;

                    var authToken = JsonConvert.DeserializeObject(Preferences.Get(Constants.RestStrings.Token, String.Empty)).ToString();
                    _authenticationService.Logout(_logoutSubscriber, authToken);
                }
            });
        }

        private async Task MakeRequestWithPhoto(MediaFile _image)
        {
            if (_image == null) return;

            IsBusy = true;

            var content = new MultipartFormDataContent();
            content.Add(new StreamContent(_image.GetStream()), "\"file\"", $"\"{_image.Path}\"");

            var httpClient = new System.Net.Http.HttpClient();

            var authToken = JsonConvert.DeserializeObject(Preferences.Get(Constants.RestStrings.Token, String.Empty)).ToString();
            httpClient.DefaultRequestHeaders.Add("x-auth", authToken);

            var url = "http://nodetesseract.tk/api/coverImage";
            var responseMsg = await httpClient.PostAsync(url, content);

            if (responseMsg.IsSuccessStatusCode)
            {
                var responseContent = await responseMsg.Content.ReadAsStringAsync();
                UserDataModel receivedUser = (UserDataModel)JsonConvert.DeserializeObject<UserDataModel>(responseContent);
                var userDomainModel = new UserModelConverter().Transform(receivedUser);
                var userViewModel = new UserViewModel()
                {
                    User = userDomainModel
                };

                UserViewModel = userViewModel;
                Preferences.Set(Constants.RestStrings.LoggedInUser, JsonConvert.SerializeObject(userViewModel));
            }
            else
            {
                await Alert.ShowAsync("We are very sorry..", "Something unnexpected happened. Please try again later", "Ok");
            }

            IsBusy = false;
        }

        private class LogoutSubscriber : IObserver<object>
        {
            private UserProfilePageViewModel _sideMenuViewModel;

            public LogoutSubscriber(UserProfilePageViewModel viewModel)
            {
                _sideMenuViewModel = viewModel;
            }

            public void OnCompleted()
            {
                Logger.Instance.Info("DeviceManager. Logout Request Complete.");

                _sideMenuViewModel.IsBusy = false;
            }

            public void OnError(Exception error)
            {
                Logger.Instance.Error("DeviceManager. Logout Request Error.");
                _sideMenuViewModel.IsBusy = false;
            }

            public void OnNext(object value)
            {
                Logger.Instance.Info("DeviceManager. Logout Response.");

                Preferences.Set(Constants.RestStrings.Token, string.Empty);
                Preferences.Set(Constants.RestStrings.LoggedInUser, string.Empty);

                Device.BeginInvokeOnMainThread(() => _sideMenuViewModel.Navigation.ReplaceRootPage(new NavigationPage(new LoginPage())));
            }
        }

        private class UpdateProfileSubscriber : IObserver<UserDomainModel>
        {
            private UserProfilePageViewModel _userProfilePageViewModel;

            public UpdateProfileSubscriber(UserProfilePageViewModel viewModel)
            {
                _userProfilePageViewModel = viewModel;
            }

            public void OnCompleted()
            {
                Logger.Instance.Info("DeviceManager. Update Profile Request Complete.");

                _userProfilePageViewModel.IsBusy = false;
            }

            public void OnError(Exception error)
            {
                Logger.Instance.Error("DeviceManager. Update Profile Request Error.");
                _userProfilePageViewModel.IsBusy = false;
            }

            public void OnNext(UserDomainModel value)
            {
                Logger.Instance.Error("DeviceManager. Update Profile Request Error.");
                Preferences.Set(Constants.RestStrings.Token, JsonConvert.SerializeObject(value.Token));

                var userViewModel = new UserViewModel() { User = value };
                _userProfilePageViewModel.UserViewModel = userViewModel;
                _userProfilePageViewModel.IsInfoDirty = false;
                Preferences.Set(Constants.RestStrings.LoggedInUser, JsonConvert.SerializeObject(userViewModel));
            }
        }
    }
}
