﻿using System;
using Licenta.Domain.Models;

namespace Licenta.Presentation.ViewModels
{
    public class UserViewModel
    {
        public UserDomainModel User { get; set; }

        public string PhotoURL => User.PhotoURL == null ? "profileimage" : User.PhotoURL;

        public string InitialsOfName
        {
            get
            {
                var initials = User.Name.Split(' ');

                if(initials.Length == 1)
                {
                    var name = initials[0];

                    return name[0].ToString();
                }
                else
                {
                    var firstName = initials[0];
                    var lastName = initials[1];

                    return firstName[0].ToString() + lastName[0].ToString();
                }
            }
        }
    }
}
