﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using Licenta.Data.Repository;
using Licenta.Data.Utilities;
using Licenta.Domain.Models;
using Licenta.Domain.Services;
using Licenta.Domain.Utilities;
using Licenta.Presentation.Utilities;
using Licenta.Presentation.Views;
using Licenta.Utilities;
using Newtonsoft.Json;
using Refit;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Licenta.Presentation.ViewModels
{
    public class LoginPageViewModel : BaseViewModel
    {
        #region COMMANDS

        public ICommand ChangeStateButtonTappedCommand { get; private set; }
        public ICommand AuthButtonTappedCommand { get; private set; }

        #endregion

        #region PROPERTIES

        private AuthenticationService _authenticationService;
        private AuthenticationSubscriber _loginSubscriber;

        public bool ShouldFacebookButtonBeEnabled { get; set; }
        public bool IsLoginState { get; set; }

        public string Email { get; set; }
        public string Password { get; set; }
        public string DisplayName { get; set; }

        public string BottomLabelText           => IsLoginState ? "Don't have an account?" : "Already have an account?";
        public string ChangeStateButtonText     => IsLoginState ? "Register Now" : "Go to Login";
        public string LoginOrRegisterButtonText => IsLoginState ? "Login" : "Create";

        #endregion

        #region CTOR

        public LoginPageViewModel()
        {
            ChangeStateButtonTappedCommand = new Command(() => Handle_ChangeStateOfView());
            AuthButtonTappedCommand = new Command(() => Handle_AuthButtonTapped());

            _authenticationService = new AuthenticationService(new AuthenticationApiRepository());
            _loginSubscriber = new AuthenticationSubscriber(this);

            IsLoginState = true;
            ShouldFacebookButtonBeEnabled = true;

            App.PostSuccessFacebookAction = token =>
            {
                ShouldFacebookButtonBeEnabled = false;
                IsBusy = true;

                _authenticationService.LoginWithFacebook(_loginSubscriber, token);
            };
        }

        #endregion

        #region PRIVATE METHODS

        private void Handle_AuthButtonTapped()
        {
            IsBusy = true;
            ShouldFacebookButtonBeEnabled = true;

            try
            {
                ValidateCredentials();

                var credentials = new CredentialsDomainModel
                {
                    Email = Email,
                    DisplayName = DisplayName,
                    Password = Password
                };

                if (IsLoginState)
                {
                    _authenticationService.Login(_loginSubscriber, credentials);
                }
                else
                {
                    _authenticationService.Register(_loginSubscriber, credentials);
                }
            }
            catch (AuthenticationException e)
            {
                Alert.ShowAsync("Error", e.Message, "Ok");
                IsBusy = false;
            }
        }

        private void ValidateCredentials()
        {
            int _mandatoryLengthOfEachEntry = 4;

            if(Email.IsNullOrEmpty())
            {
                throw new AuthenticationException("Email should not be left empty");
            }

            if (Password.IsNullOrEmpty())
            {
                throw new AuthenticationException("Password should not be left empty");
            }

            if (Email.Length < _mandatoryLengthOfEachEntry)
            {
                throw new AuthenticationException("Email should be at least 4 characters long");
            }

            if (!OtherMethods.IsValidEmail(Email))
            {
                throw new AuthenticationException("Email entry should be filled with a valid email address");
            }

            if (!IsLoginState)
            {
                if (DisplayName.IsNullOrEmpty())
                {
                    throw new AuthenticationException("Display Name should not be left empty");
                }

                if (DisplayName.Length < _mandatoryLengthOfEachEntry)
                {
                    throw new AuthenticationException("Display Name should be at least 4 characters long");
                }
            }

            if (Password.Length < _mandatoryLengthOfEachEntry)
            {
                throw new AuthenticationException("Password should be at least 4 characters long");
            }
        }

        private void Handle_ChangeStateOfView()
        {
            IsLoginState = !IsLoginState;
        }

        #endregion

        #region LoginSubscriber

        private class AuthenticationSubscriber : IObserver<UserDomainModel>
        {
            private LoginPageViewModel _loginPageViewModel;

            public AuthenticationSubscriber(LoginPageViewModel viewModel)
            {
                _loginPageViewModel = viewModel;
            }

            public void OnCompleted()
            {
                Logger.Instance.Info("Licenta. Authentication Request Complete.");

                Device.BeginInvokeOnMainThread(() =>
                {
                    var rootPage = new RootPage();
                    _loginPageViewModel.Navigation.ReplaceRootPage(rootPage);
                    _loginPageViewModel.IsBusy = false;
                });
            }

            public void OnError(Exception error)
            {
                Logger.Instance.Info("Licenta. Authentication Request Error.");

                _loginPageViewModel.IsBusy = false;
                _loginPageViewModel.ShouldFacebookButtonBeEnabled = false;

                if (error is ApiException apiError)
                {
                    var parsedError = apiError.GetContentAs<Dictionary<String, String>>();
                    var content = parsedError["err"];
                    Device.BeginInvokeOnMainThread(() => _loginPageViewModel.Alert.ShowAsync("Error", content, "Ok"));
                }
                else
                {
                    Device.BeginInvokeOnMainThread(() => _loginPageViewModel.Alert.ShowAsync("We're sorry!", "An unnexpected error happened. Totally our fault", "Ok"));
                }
            }

            public void OnNext(UserDomainModel value)
            {
                Preferences.Set(Constants.RestStrings.Token, JsonConvert.SerializeObject(value.Token));

                var userViewModel = new UserViewModel() { User = value };
                Preferences.Set(Constants.RestStrings.LoggedInUser, JsonConvert.SerializeObject(userViewModel));
            }
        }

        #endregion
    }
}
