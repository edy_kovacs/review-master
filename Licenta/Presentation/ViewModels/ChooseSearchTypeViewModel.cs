﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Licenta.Data.Models;
using Licenta.Presentation.Views.Search;
using Licenta.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Xamarin.Forms;

namespace Licenta.Presentation.ViewModels
{
    public class ChooseSearchTypeViewModel : BaseViewModel
    {
        public ICommand OnBackPressedCommand { get; set; }
        public ICommand ContinueButtonCommand { get; set; }

        public Frame SelectedFrame { get; set; }
        public bool IsContinueButtonVisible => SelectedFrame != null;

        public string SelectedSearchType { get; set; } = null;

        public ChooseSearchTypeViewModel()
        {
            OnBackPressedCommand = new Command(() => OnBackPressed());
            ContinueButtonCommand = new Command(async () => await OnContinuePressedAsync());
        }

        private async Task OnContinuePressedAsync()
        {
            if (SelectedFrame.ClassId.Equals("FirstFrame"))
            {
                await Navigation.PushAsync(new SearchPage());
            }
            else if (SelectedFrame.ClassId.Equals("SecondFrame"))
            {
                await Navigation.PushAsync(new QrCodeSearchPage());
            }
            else if (SelectedFrame.ClassId.Equals("ThirdFrame"))
            {
                await Navigation.PushAsync(new OcrSearchPage());
            }
            else
            {
                return;
            }
        }


        private void OnBackPressed()
        {
            Navigation.PopAsync();
        }

        public void HandleFrameSelection(Frame clickedFrame)
        {
            if (SelectedFrame == null)
            {
                SelectedFrame = clickedFrame;
                SelectFrame();
            }
            else
            {
                if (SelectedFrame.Equals(clickedFrame))
                {
                    DeselectFrame();
                    SelectedFrame = null;
                }
                else
                {
                    DeselectFrame();
                    SelectedFrame = clickedFrame;
                    SelectFrame();
                }
            }
        }

        private void SelectFrame()
        {
            SelectedFrame.BackgroundColor = Constants.Colors.CustomTeal;
            var children = SelectedFrame.Children;
            var containerStackLayout = (StackLayout)children[0];

            var image = (Image)containerStackLayout.Children[0];
            var fileSource = image.Source.ToString();
            var fileName = fileSource.Split(' ')[1];

            if (fileName.Contains(".png"))
            {
                fileName = fileName.Split('.')[0];
            }

            image.Source = fileName + "white.png";

            var containedStackLayout = (StackLayout)containerStackLayout.Children[1];
            var firstLabel = (Label)containedStackLayout.Children[0];
            firstLabel.TextColor = Color.White;

            var secondLabel = (Label)containedStackLayout.Children[1];
            secondLabel.TextColor = Color.White;
        }

        private void DeselectFrame()
        {
            SelectedFrame.BackgroundColor = Color.White;
            var children = SelectedFrame.Children;
            var containerStackLayout = (StackLayout)children[0];

            var image = (Image)containerStackLayout.Children[0];
            var fileSource = image.Source.ToString();
            var fileName = fileSource.Split(' ')[1];

            fileName = fileName.Substring(0, fileName.Length - 9);
            image.Source = fileName + ".png";

            var containedStackLayout = (StackLayout)containerStackLayout.Children[1];
            var firstLabel = (Label)containedStackLayout.Children[0];
            firstLabel.TextColor = Color.Black;

            var secondLabel = (Label)containedStackLayout.Children[1];
            secondLabel.TextColor = Color.DarkGray;
        }
    }
}
