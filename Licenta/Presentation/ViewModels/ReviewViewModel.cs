﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Licenta.Domain.Models;
using Licenta.Utilities;
using Newtonsoft.Json;
using PropertyChanged;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Licenta.Presentation.ViewModels
{
    public class ReviewViewModel : BaseViewModel
    {
        public ReviewDomainModel Review { get; set; }

        public ObservableCollection<string> Likes = new ObservableCollection<string>();
        public ICommand LikeTappedCommand { get; private set; }

        public UserViewModel LoggedInUser { get; set; } = JsonConvert.DeserializeObject<UserViewModel>(Preferences.Get(Constants.RestStrings.LoggedInUser, String.Empty));
        public int LikesCount => Likes.Count;
        public bool IsLikedByLoggedUser => LikesCount == 0 ? false : Likes.Contains(LoggedInUser.User.Id);
        public string ThumbUpImageSource => IsLikedByLoggedUser ? "likegreen.png" : "like.png";
        public double ThumbUpOpacity => IsLikedByLoggedUser ? 1.0 : 0.5;

        public Thickness ComputedMargin { get; set; } = new Thickness(12);

        public ReviewViewModel(ReviewDomainModel review)
        {
            Review = review;

            Likes = new ObservableCollection<string>(Review?.Likes);

            LikeTappedCommand = new Command(() => LikeTapped());
        }

        private void LikeTapped()
        {
            if (IsLikedByLoggedUser)
            {
                Likes.Remove(LoggedInUser.User.Id);

                OnPropertyChanged("NumberOfUsersLikedThis", LikesCount, LikesCount - 1);
                OnPropertyChanged("ThumbUpImageSource", LikesCount, LikesCount - 1);
                OnPropertyChanged("ThumbUpOpacity", LikesCount, LikesCount - 1);

                //TODO: UnLike Request
            }
            else
            {
                Likes.Add(LoggedInUser.User.Id);

                OnPropertyChanged("NumberOfUsersLikedThis", LikesCount - 1, LikesCount);
                OnPropertyChanged("ThumbUpImageSource", LikesCount - 1, LikesCount);
                OnPropertyChanged("ThumbUpOpacity", LikesCount, LikesCount - 1);

                //TODO: Like Request
            }
        }

        public string RatingString => $"Rating: {Review?.Rating}/5 ⭐";
        public string DateOfReview => Review?.DateOfReview;

        public FormattedString AuthorAndDateString
        {
            get
            {
                var authoredSpan = new Span() { Text = "Authored by ", FontAttributes = FontAttributes.Italic, TextColor = Constants.Colors.GreenishBlack };
                var authorSpan = new Span() { Text = Review?.Author, FontAttributes = FontAttributes.Bold, TextColor = Constants.Colors.GreenishBlack };
                var dateSpan = new Span() { Text = $" in {DateOfReview}", FontAttributes = FontAttributes.Italic, TextColor = Constants.Colors.GreenishBlack };

                var formattedString = new FormattedString();
                formattedString.Spans.Add(authoredSpan);
                formattedString.Spans.Add(authorSpan);
                formattedString.Spans.Add(dateSpan);

                return formattedString;
            }
        }

        public FormattedString NumberOfUsersLikedThis
        {
            get
            {
                var thisReviewSpan = new Span() { Text = "This review was liked by", TextColor = Constants.Colors.GreenishBlack };
                var numberOfLikesSpan = new Span() { Text = $" {LikesCount} ", FontAttributes = FontAttributes.Bold, TextColor = Constants.Colors.GreenishBlack };
                var usersSpan = new Span() { Text = "users", TextColor = Constants.Colors.GreenishBlack };

                var formattedString = new FormattedString();
                formattedString.Spans.Add(thisReviewSpan);
                formattedString.Spans.Add(numberOfLikesSpan);
                formattedString.Spans.Add(usersSpan);

                return formattedString;
            }
        }
    }
}
