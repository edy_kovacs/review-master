﻿using System.ComponentModel;
using Licenta.Presentation.Utilities;

namespace Licenta.Presentation.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public bool IsBusy { get; set; }
        public INavigationService Navigation => DependencyResolver.NavigationService;
        public IAlertService Alert => DependencyResolver.AlertService;

        public void OnPropertyChanged(string propertyName, object before, object after)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
