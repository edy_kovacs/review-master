﻿using System;
using System.Windows.Input;
using Licenta.Data.Repository;
using Licenta.Domain.Models;
using Licenta.Domain.Services;
using Licenta.Domain.Utilities;
using Licenta.Utilities;
using Newtonsoft.Json;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Licenta.Presentation.ViewModels
{
    public class ProductDetailsPageViewModel : BaseViewModel
    {
        public DetailedProductViewModel DetailedProductViewModel { get; set; }

        public ICommand OnBackPressedCommand { get; private set; }

        DetailedProductSubscriber _detailedProductSubscriber;
        DetailedProductService _detailedProductService;

        public ProductDetailsPageViewModel()
        {
            _detailedProductSubscriber = new DetailedProductSubscriber(this);
            _detailedProductService = new DetailedProductService(new DetailedProductApiRepository());

            OnBackPressedCommand = new Command(() => OnBackPressed());
        }

        private void OnBackPressed()
        {
            Navigation.PopAsync();
        }

        public void FetchProductDetails(string productId)
        {
            var authToken = JsonConvert.DeserializeObject(Preferences.Get(Constants.RestStrings.Token, String.Empty)).ToString();
            _detailedProductService.GetProduct(_detailedProductSubscriber, productId, authToken);
            IsBusy = true;
        }

        private class DetailedProductSubscriber : IObserver<DetailedProductDomainModel>
        {
            private ProductDetailsPageViewModel _productDetailsPageViewModel;

            public DetailedProductSubscriber(ProductDetailsPageViewModel viewModel)
            {
                _productDetailsPageViewModel = viewModel;
            }

            public void OnCompleted()
            {
                Logger.Instance.Info("Licenta. Get Detailed Product Request Complete.");
                _productDetailsPageViewModel.IsBusy = false;
            }

            public void OnError(Exception error)
            {
                Logger.Instance.Error("Licenta. Get Detailed Product Request Error.");
                Logger.Instance.Error(error.Message);
                _productDetailsPageViewModel.IsBusy = false;
            }

            public void OnNext(DetailedProductDomainModel value)
            {
                Logger.Instance.Info("Licenta. Get Detailed Product Response.");

                // Some strange bug if this is removed
                Device.BeginInvokeOnMainThread(() =>
                {
                    _productDetailsPageViewModel.DetailedProductViewModel = new DetailedProductViewModel(value);
                });
            }
        }
    }
}
