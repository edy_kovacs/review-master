﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Licenta.Data.Utilities;
using Licenta.Domain.Models;
using Licenta.Utilities;
using Xamarin.Forms;

namespace Licenta.Presentation.ViewModels
{
    public class DetailedProductViewModel : BaseViewModel
    {
        public DetailedProductDomainModel DetailedProduct { get; set; }
        public string PhotoURL => DetailedProduct?.PhotoURL ?? "noimage";
        public string ReviewsCountString => DetailedProduct?.Reviews.Count + " reviews";
        public ObservableCollection<ReviewViewModel> Reviews { get; set; } = new ObservableCollection<ReviewViewModel>();

        public DetailedProductViewModel(DetailedProductDomainModel detailedProduct)
        {
            DetailedProduct = detailedProduct;

            detailedProduct.Reviews.ForEach((obj) =>
            {
                Reviews.Add(new ReviewViewModel(obj));
            });

            if (Reviews.Count > 0)
            {
                var firstElementMargins = new Thickness(14, 28, 14, 14);
                var firstReview = Reviews.First();
                firstReview.ComputedMargin = firstElementMargins;
            }
        }

        public ObservableCollection<Grouping<string, ProductCharacteristic>> GroupedCharacteristics
        {
            get
            {
                if (DetailedProduct == null) return null;
                var sorted = from characteristic in DetailedProduct.Characteristics
                             orderby characteristic.Key
                             group characteristic by characteristic.Subcategory into characteristicGroup
                             select new Grouping<string, ProductCharacteristic>(characteristicGroup.Key, characteristicGroup);

                return new ObservableCollection<Grouping<string, ProductCharacteristic>>(sorted);
            }
        }
    }
}
