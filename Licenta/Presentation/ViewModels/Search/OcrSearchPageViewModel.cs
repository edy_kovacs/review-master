﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Windows.Input;
using Licenta.Data.Models;
using Licenta.Presentation.Utilities;
using Licenta.Presentation.Views.Search;
using Licenta.Utilities;
using Newtonsoft.Json;
using Plugin.Media.Abstractions;
using Xamarin.Forms;

namespace Licenta.Presentation.ViewModels.Search
{
    public class OcrSearchPageViewModel : BaseViewModel
    {
        private Func<IAlertService, Task<MediaFile>> _pickPhotoFunc;
        private Func<IAlertService, Task<MediaFile>> _takePhotoFunc;

        private int _defaultWordsTaken = 10;
        private int _maxWordsCount = 30;
        private int _indexOfEnglishLang = 4;
        private int _defaultResizeWidth = 768;
        private int _defaultResizeHeight = 1024;

        public ICommand TakePhotoAndGetOcrResultsCommand { get; private set; }
        public ICommand PickPhotoAndGetOcrResultsCommand { get; private set; }
        public ICommand OnBackPressedCommand { get; private set; }

        public string MaxWords { get; set; }
        public bool ShouldBeResized { get; set; }
        public string ResizeWidth { get; set; }
        public string ResizeHeight { get; set; }

        public Language SelectedLanguage { get; set; }
        public List<Language> SupportedLanguages { get; set; }

        public OcrSearchPageViewModel()
        {
            TakePhotoAndGetOcrResultsCommand = new Command(async () => await GetPhotoAndExecuteSearchAsync(_takePhotoFunc));
            PickPhotoAndGetOcrResultsCommand = new Command(async () => await GetPhotoAndExecuteSearchAsync(_pickPhotoFunc));
            OnBackPressedCommand = new Command(async () => await OnBackPressedAsync());

            _pickPhotoFunc = new Func<IAlertService, Task<MediaFile>>(OtherMethods.PickPhotoAsync);
            _takePhotoFunc = new Func<IAlertService, Task<MediaFile>>(OtherMethods.TakePhotoAsync);

            PopulateSupportedLanguages();
        }

        private async Task GetPhotoAndExecuteSearchAsync(Func<IAlertService, Task<MediaFile>> getPhotoFunc)
        {
            var wordsCount = MaxWords.IsNullOrEmpty() ? _defaultWordsTaken : Int32.Parse(MaxWords); ;

            if (wordsCount > _maxWordsCount || wordsCount < 1)
            {
                await Alert.ShowAsync("Error - can not display so many words", "Max words should set between 1 and 30 in order for the search to be accurate", "Ok");
                IsBusy = false;
                return;
            }

            IsBusy = true;

            var photo = await getPhotoFunc(Alert);

            if (photo == null)
            {
                IsBusy = false;
                return;
            }

            await ExecuteSearchAsync(photo, wordsCount, SelectedLanguage);
        }

        private void PopulateSupportedLanguages()
        {
            SupportedLanguages = new List<Language>()
            {
                new Language("Czech", "cs"),
                new Language("Danish", "da"),
                new Language("Dutch", "nl"),
                new Language("English", "en"),
                new Language("French", "fr"),
                new Language("German", "de"),
                new Language("Italian", "it"),
                new Language("Romanian", "ro"),
                new Language("Spanish", "es"),
                new Language("Swedish", "sv"),
                new Language("Russian", "ru")
            };
        }

        private async Task ExecuteSearchAsync(MediaFile photo, int wordsCount, Language selectedLanguage)
        {
            int width, height;
            bool shouldResize = false;
            var ocrLanguage = selectedLanguage == null ? SupportedLanguages[_indexOfEnglishLang].Value : selectedLanguage.Value;

            if (ShouldBeResized)
            {
                shouldResize = true;
                width = ResizeWidth == null ? _defaultResizeWidth : Int32.Parse(ResizeWidth);
                height = ResizeHeight == null ? _defaultResizeHeight : Int32.Parse(ResizeHeight);
            }
            else
            {
                width = 0;
                height = 0;
            }

            var foundWords = await OcrService.GetOcrResultAsync(photo, ocrLanguage, shouldResize, width, height, Alert);

            if (foundWords == null || foundWords?.Count == 0)
            {
                await Alert.ShowAsync("No words have been identified", "Please consider to try again with another picture", "Ok");
                IsBusy = false;
                return;
            }

            var words = foundWords?.Count > wordsCount ? foundWords.GetRange(0, wordsCount) : foundWords;
            await Navigation.PushAsync(new SelectWordsSearchPage(words));
            IsBusy = false;
        }

        private async Task OnBackPressedAsync()
        {
            await Navigation.PopAsync();
        }

    }
}
