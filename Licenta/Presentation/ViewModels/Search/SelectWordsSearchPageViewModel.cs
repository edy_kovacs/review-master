﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Licenta.Data.Repository;
using Licenta.Domain.Models;
using Licenta.Domain.Services;
using Licenta.Domain.Utilities;
using Licenta.Utilities;
using Newtonsoft.Json;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Licenta.Presentation.ViewModels.Search
{
    public class SelectWordsSearchPageViewModel : BaseViewModel
    {
        private SearchResultsSubscriber _searchResultsSubscriber;
        private RecentSearchesService _recentSearchesService;           // Ignore the naming of the class
        private char[] EscapedCharacters = new char[] { '.', ',', ':', ';', '-', '?', '!', '_' };

        #region Commands

        public ICommand WordSelectedCommand { get; private set; }
        public ICommand ProductSelectedCommand { get; private set; }
        public ICommand FetchSearchResultsCommand { get; private set; }
        public ICommand OnBackPressedCommand { get; private set; }

        #endregion

        #region Properties

        public ObservableCollection<WordViewModel> Words { get; set; } = new ObservableCollection<WordViewModel>();
        public ObservableCollection<ProductViewModel> SearchResults { get; set; } = new ObservableCollection<ProductViewModel>();

        private string _searchText = String.Empty;
        public string SearchText
        {
            get => _searchText;
            set
            {
                _searchText = value;

                if (String.IsNullOrEmpty(_searchText))
                {
                    SearchResults = new ObservableCollection<ProductViewModel>();
                }
                else
                {
                    FetchSearchResultsCommand.Execute(_searchText);
                }
            }
        }

        public bool ShouldDisplayEmptyState => (String.IsNullOrEmpty(SearchText) && !IsBusy && SearchResults?.Count == 0) || (!IsBusy && SearchResults?.Count == 0);

        #endregion

        #region CTOR

        public SelectWordsSearchPageViewModel(List<string> words)
        {
            WordSelectedCommand = new Command((obj) => Handle_WordSelected(obj as WordViewModel));
            FetchSearchResultsCommand = new Command(() => FetchSearchResults());
            OnBackPressedCommand = new Command(async () => await Handle_OnBackPressedAsync());
            ProductSelectedCommand = new Command(async (obj) => await Handle_OnProductSelectedAsync(obj as ProductViewModel));

            _searchResultsSubscriber = new SearchResultsSubscriber(this);
            _recentSearchesService = new RecentSearchesService(new RecentSearchesApiRepository());

            PopulateWordsCollection(words);
        }

        #endregion

        #region Private Methods

        private async Task Handle_OnProductSelectedAsync(ProductViewModel productViewModel)
        {
            if (productViewModel == null)
            {
                return;
            }

            await Navigation.PushAsync(new Views.ProductDetails.ProductDetailsPage(productViewModel));

            MessagingCenter.Send<Application, ProductViewModel>(Application.Current, "ProductSelected", productViewModel);
        }

        private async Task Handle_OnBackPressedAsync()
        {
            await Navigation.PopToRootAsync();
        }

        private void FetchSearchResults()
        {
            IsBusy = true;
            SearchResults.Clear();

            var authToken = JsonConvert.DeserializeObject(Preferences.Get(Constants.RestStrings.Token, String.Empty)).ToString();
            var searchText = new Dictionary<string, string>();
            var searchTextValue = SearchText.Trim(new char[] { ',' });
            var searchTextNoCommas = searchTextValue.Replace(',', ' ');
            searchText.Add("name", searchTextNoCommas);

            _recentSearchesService.FetchSearchResults(_searchResultsSubscriber, authToken, searchText);
        }

        private void Handle_WordSelected(WordViewModel wordViewModel)
        {
            UpdateSearchText(wordViewModel);
            wordViewModel.IsSelected = !wordViewModel.IsSelected;
        }

        private void PopulateWordsCollection(List<string> words)
        {
            words.ForEach(word =>
            {
                word = word.Trim(EscapedCharacters);
                if (!Words.Select((wrd) => wrd.Text).ToList().Contains(word))
                {
                    Words.Add(new WordViewModel(word));
                }
            });
        }

        private void UpdateSearchText(WordViewModel wordViewModel)
        {
            if (wordViewModel.IsSelected)
            {
                SearchText = SearchText.Replace(", " + wordViewModel.Text, String.Empty);
                SearchText = SearchText.Replace(wordViewModel.Text + ", ", String.Empty);
                SearchText = SearchText.Replace(wordViewModel.Text, String.Empty);
            }
            else
            {
                if (SearchText.Length == 0)
                {
                    SearchText = wordViewModel.Text;
                }
                else
                {
                    SearchText += ", " + wordViewModel.Text;
                }
            }
        }

        #endregion

        #region SearchResultsSubscriber

        private class SearchResultsSubscriber : IObserver<List<ProductDomainModel>>
        {
            private SelectWordsSearchPageViewModel _selectWordsSearchPageViewModel;

            public SearchResultsSubscriber(SelectWordsSearchPageViewModel viewModel)
            {
                _selectWordsSearchPageViewModel = viewModel;
            }

            public void OnCompleted()
            {
                Logger.Instance.Info("Licenta. Searche Results Request Complete.");
                _selectWordsSearchPageViewModel.IsBusy = false;

                if (_selectWordsSearchPageViewModel.SearchResults.Count == 0)
                {
                    _selectWordsSearchPageViewModel.SearchResults = new ObservableCollection<ProductViewModel>();
                }
            }

            public void OnError(Exception error)
            {
                Logger.Instance.Error("Licenta. Searche Results Request Error.");
                Logger.Instance.Error(error.Message);
                _selectWordsSearchPageViewModel.IsBusy = false;
            }

            public void OnNext(List<ProductDomainModel> value)
            {
                Logger.Instance.Info("Licenta. Searche Results Response.");

                var searchResultsList = value.Select(productDomainModel => new ProductViewModel(productDomainModel)).ToList();
                foreach (var searchResult in searchResultsList)
                {
                    _selectWordsSearchPageViewModel.SearchResults.Add(searchResult);
                }
            }
        }

        #endregion
    }

}
