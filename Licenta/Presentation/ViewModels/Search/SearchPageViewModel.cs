﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Licenta.Data.Repository;
using Licenta.Domain.Models;
using Licenta.Domain.Services;
using Licenta.Domain.Utilities;
using Licenta.Utilities;
using Newtonsoft.Json;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Licenta.Presentation.ViewModels.Search
{
    public class SearchPageViewModel : BaseViewModel
    {
        private SearchResultsSubscriber _searchResultsSubscriber;
        private RecentSearchesService _recentSearchesService;       // Ignore the naming of the class

        public ICommand OnBackPressedCommand { get; private set; }
        public ICommand ClearButtonTappedCommand { get; private set; }
        public ICommand ProductSelectedCommand { get; private set; }
        public ICommand SearchSuggestionsCommand { get; private set; }

        public ObservableCollection<ProductViewModel> SearchResults { get; set; } = new ObservableCollection<ProductViewModel>();
        public string SearchText { get; set; } = String.Empty;
        public bool ShouldDisplayClearButton => !SearchText.Equals(String.Empty);
        public bool ShouldDisplayEmptyState { get; set; } = true;
        public Thickness EntryMargin => Device.RuntimePlatform == Device.iOS ? new Thickness(0) : new Thickness(0, 0, 0, -6);

        public SearchPageViewModel()
        {
            OnBackPressedCommand = new Command(async () => await OnBackPressedAsync());
            ClearButtonTappedCommand = new Command(() => ClearButtonTapped());
            ProductSelectedCommand = new Command(async (productViewModel) => await OnProductSelectedAsync((ProductViewModel)productViewModel));
            SearchSuggestionsCommand = new Command(() => UpdateSearchResultsList());

            _searchResultsSubscriber = new SearchResultsSubscriber(this);
            _recentSearchesService = new RecentSearchesService(new RecentSearchesApiRepository());
        }

        private void UpdateSearchResultsList()
        {
            if (String.IsNullOrWhiteSpace(SearchText))
            {
                ShouldDisplayEmptyState = true;
            }
            else
            {
                IsBusy = true;
                ShouldDisplayEmptyState = false;
                SearchResults = new ObservableCollection<ProductViewModel>();

                var authToken = JsonConvert.DeserializeObject(Preferences.Get(Constants.RestStrings.Token, String.Empty)).ToString();
                var searchText = new Dictionary<string, string>();
                searchText.Add("name", SearchText);

                _recentSearchesService.FetchSearchResults(_searchResultsSubscriber, authToken, searchText);
            }
        }

        private async Task OnProductSelectedAsync(ProductViewModel productViewModel)
        {
            if (productViewModel == null) return;

            await Navigation.PushAsync(new Views.ProductDetails.ProductDetailsPage(productViewModel));

            MessagingCenter.Send<Application, ProductViewModel>(Application.Current, "ProductSelected", productViewModel);
        }

        private void ClearButtonTapped()
        {
            SearchText = String.Empty;
            SearchResults = new ObservableCollection<ProductViewModel>();
        }

        private async Task OnBackPressedAsync()
        {
            await Navigation.PopAsync();
        }

        private class SearchResultsSubscriber : IObserver<List<ProductDomainModel>>
        {
            private SearchPageViewModel _searchPageViewModel;

            public SearchResultsSubscriber(SearchPageViewModel viewModel)
            {
                _searchPageViewModel = viewModel;
            }

            public void OnCompleted()
            {
                Logger.Instance.Info("Licenta. Searche Results Request Complete.");
                _searchPageViewModel.IsBusy = false;
            }

            public void OnError(Exception error)
            {
                Logger.Instance.Error("Licenta. Searche Results Request Error.");
                Logger.Instance.Error(error.Message);
                _searchPageViewModel.IsBusy = false;
            }

            public void OnNext(List<ProductDomainModel> value)
            {
                Logger.Instance.Info("Licenta. Searche Results Response.");

                // Some strange bug if this is removed
                Device.BeginInvokeOnMainThread(() =>
                {
                    var searchResultsList = value.Select(productDomainModel => new ProductViewModel(productDomainModel)).ToList();
                    searchResultsList.ForEach(searchResult => _searchPageViewModel.SearchResults.Add(searchResult));
                    _searchPageViewModel.ShouldDisplayEmptyState = searchResultsList.Count == 0;
                });
            }
        }
    }
}
