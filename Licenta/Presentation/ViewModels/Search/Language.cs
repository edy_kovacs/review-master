﻿using System;

namespace Licenta.Presentation.ViewModels.Search
{
    public class Language
    {
        public string PrettyName { get; set; }
        public string Value { get; set; }

        public Language(string prettyName, string value)
        {
            PrettyName = prettyName;
            Value = value;
        }

        public override string ToString()
        {
            return PrettyName;
        }
    }
}
