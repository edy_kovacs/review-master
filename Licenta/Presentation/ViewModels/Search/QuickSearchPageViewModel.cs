﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Licenta.Presentation.Utilities;
using Licenta.Presentation.Views.Search;
using Licenta.Utilities;
using Plugin.Media.Abstractions;
using Xamarin.Forms;

namespace Licenta.Presentation.ViewModels.Search
{
    public class QuickSearchPageViewModel : BaseViewModel
    {
        public ICommand TakePhotoAndGetOcrResultsCommand { get; private set; }
        public ICommand OnBackPressedCommand { get; private set; }

        public QuickSearchPageViewModel()
        {
            TakePhotoAndGetOcrResultsCommand = new Command(async () => await GetPhotoAndExecuteSearchAsync());
            OnBackPressedCommand = new Command(async () => await OnBackPressedAsync());
        }

        private async Task OnBackPressedAsync()
        {
            await Navigation.PopAsync();
        }

        private async Task GetPhotoAndExecuteSearchAsync()
        {
            IsBusy = true;

            var photo = await OtherMethods.TakePhotoAsync(Alert); 

            if (photo == null)
            {
                IsBusy = false;
                return;
            }

            await ExecuteSearchAsync(photo);
        }

        private async Task ExecuteSearchAsync(MediaFile photo, int wordsCount = 15)
        {
            int width = 0, height = 0;
            bool shouldResize = false;
            var ocrLanguage = "en";

            var foundWords = await OcrService.GetOcrResultAsync(photo, ocrLanguage, shouldResize, width, height, Alert);

            if (foundWords == null || foundWords?.Count == 0)
            {
                await Alert.ShowAsync("No words have been identified", "Please consider to try again with another picture", "Ok");
                IsBusy = false;
                return;
            }

            var words = foundWords?.Count > wordsCount ? foundWords.GetRange(0, wordsCount) : foundWords;
            await Navigation.PushAsync(new SelectWordsSearchPage(words));
            IsBusy = false;
        }
    }
}
