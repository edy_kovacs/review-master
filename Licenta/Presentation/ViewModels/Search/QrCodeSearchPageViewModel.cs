﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using Licenta.Presentation.Views.Search;
using Xamarin.Forms;

namespace Licenta.Presentation.ViewModels.Search
{
    public class QrCodeSearchPageViewModel : BaseViewModel
    {
        public bool ShouldScanForQrCodes { get; set; } = true;

        public ICommand OnBackPressedCommand { get; set; }

        public QrCodeSearchPageViewModel()
        {
            OnBackPressedCommand = new Command(async () => await OnBackPressedAsync());
        }

        private async Task OnBackPressedAsync()
        {
            await Navigation.PopAsync();
        }

        public async Task Handle_QrCodeScannedAsync(string qrCodeText)
        {
            IsBusy = true;
            ShouldScanForQrCodes = false;

            var separateWords = new List<string>(qrCodeText.Split(' '));
            if (separateWords.Count > 0)
            {
                Device.BeginInvokeOnMainThread(async () => await Navigation.PushAsync(new SelectWordsSearchPage(separateWords)));
            }
            else
            {
                await Alert.ShowAsync("Something went wrong", "No word has been found in the QrCode you've shown us. Please try again", "Ok");
                IsBusy = false;
                ShouldScanForQrCodes = true;
            }
        }
    }
}
