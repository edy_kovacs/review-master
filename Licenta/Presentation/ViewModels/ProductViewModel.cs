﻿using System;
using Licenta.Domain.Models;
using Xamarin.Forms;

namespace Licenta.Presentation.ViewModels
{
    public class ProductViewModel
    {
        public ProductDomainModel Product { get; set; }
        public string PhotoURL => Product.PhotoURL ?? "noimage";
        public string ReviewsCountString => Product.Reviews.Count + " reviews";

        public ProductViewModel(ProductDomainModel product)
        {
            Product = product;
        }

        public override bool Equals(object obj)
        {
            var secondObject = (ProductViewModel)obj;
            return secondObject.Product.Id.Equals(this.Product.Id);
        }
    }
}
