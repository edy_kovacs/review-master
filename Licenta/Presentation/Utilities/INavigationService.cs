﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Licenta.Presentation.Utilities
{
    public interface INavigationService
    {
        Task<Page> PopModalAsync(bool animated = true);
        Task PushModalAsync(Page page, bool animated = true);
        Task PopToRootAsync(bool animated = true);
        Task<bool> PushAsync(Page page, bool animated = true);
        Task<Page> PopAsync(bool animated = true);
        void ReplaceRootPage(Page page, bool animated = true);
    }
}
