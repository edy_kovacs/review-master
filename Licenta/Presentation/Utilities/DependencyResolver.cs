﻿namespace Licenta.Presentation.Utilities
{
    public static class DependencyResolver
    {
        public static INavigationService NavigationService { get; private set; }
        public static IAlertService AlertService { get; private set; }

        // could load the configuration from a file or use a specific IOC nugget
        public static void Configure()
        {
            NavigationService = new NavigationService();
            AlertService = new AlertService();
        }
    }
}
