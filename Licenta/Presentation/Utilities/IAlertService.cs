﻿using System.Threading.Tasks;

namespace Licenta.Presentation.Utilities
{
    public interface IAlertService
    {
        Task<bool> ShowAsync(string titleText, string subtitleText, string cancelButtonText, string confirmButtonText = null);
    }
}
