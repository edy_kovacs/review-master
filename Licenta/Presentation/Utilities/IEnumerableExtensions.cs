﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Licenta.Presentation.Utilities
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<T> WhereNotNull<T>(this IEnumerable<T> sequence)
        {
            return sequence.Where(e => e != null);
        }

        public static IEnumerable<T> WhereNotNull<T>(this IEnumerable<T?> sequence) where T : struct
        {
            return sequence.Where(e => e != null).Select(e => e.Value);
        }
    }
}
