﻿using System.Threading.Tasks;
using Licenta.Presentation.Views.CustomViews;
using Xamarin.Forms;

namespace Licenta.Presentation.Utilities
{
    public class AlertService : IAlertService
    {
        public Task<bool> ShowAsync(string titleText, string subtitleText, string cancelButtonText, string confirmButtonText = null)
        {
            var customAlert = new CustomAlert(titleText, subtitleText, cancelButtonText, confirmButtonText);

            return customAlert.ShowAsync();
        }
    }
}
