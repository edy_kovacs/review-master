﻿using System;
namespace Licenta.Presentation.Utilities
{
    public class InputsValidationException : Exception
    {
        public InputsValidationException(string message) : base(message)
        {
        }
    }
}
