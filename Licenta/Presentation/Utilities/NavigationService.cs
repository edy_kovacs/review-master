﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Licenta.Presentation.Utilities
{
    public class NavigationService : INavigationService
    {
        #region Private Fields

        static Task<Page> _lastPopAsyncTask;
        static Task _lastPushAsyncTask;

        static Page _mainPage
        {
            get
            {
                if (Application.Current.MainPage is TabbedPage tabbedPage)
                {
                    return tabbedPage.CurrentPage;
                }

                return Application.Current.MainPage;
            }
        }

        #endregion

        #region PUSH

        public async Task<bool> PushAsync(Page page, bool animated = true)
        {
            var stack = _mainPage.Navigation.NavigationStack;

            if (stack.Any() && stack.Last().GetType().Equals(page.GetType()))
            {
                return false;
            }

            if (_lastPushAsyncTask != null && !_lastPushAsyncTask.IsCompleted)
            {
                return false;
            }

            _lastPushAsyncTask = _mainPage.Navigation.PushAsync(page, animated);
            await _lastPushAsyncTask;
            return true;
        }

        public async Task PushModalAsync(Page page, bool animated = true)
        {
            var stack = _mainPage.Navigation.ModalStack;

            if (stack.Any() && stack.Last().GetType().Equals(page.GetType())) { return; }

            await _mainPage.Navigation.PushModalAsync(page, animated);
        }

        public void ReplaceRootPage(Page page, bool animated = true)
        {
            Application.Current.MainPage = page;
        }

        #endregion

        #region POP

        public async Task<Page> PopAsync(bool animated = true)
        {
            if (!IsLastPopAsyncTaskCompleted() || !_mainPage.Navigation.NavigationStack.Any())
            {
                return await _lastPopAsyncTask;
            }

            _lastPopAsyncTask = _mainPage.Navigation.PopAsync(animated);

            return await _lastPopAsyncTask;
        }

        public async Task<Page> PopModalAsync(bool animated = true)
        {
            if (!IsLastPopAsyncTaskCompleted())
            {
                return await _lastPopAsyncTask;
            }

            _lastPopAsyncTask = _mainPage.Navigation.PopModalAsync(animated);

            return await _lastPopAsyncTask;
        }

        public async Task PopToRootAsync(bool animated = true)
        {
            if (!IsLastPopAsyncTaskCompleted() || !_mainPage.Navigation.NavigationStack.Any())
            {
                return;
            }

            await _mainPage.Navigation.PopToRootAsync(animated);
        }

        bool IsLastPopAsyncTaskCompleted()
        {
            return !(_lastPopAsyncTask != null && !_lastPopAsyncTask.IsCompleted);
        }

        #endregion
    }
}
