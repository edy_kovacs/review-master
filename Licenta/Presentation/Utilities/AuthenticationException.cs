﻿using System;

namespace Licenta.Presentation.Utilities
{
    public class AuthenticationException : Exception
    {
        public AuthenticationException(string message) : base(message)
        {
        }
    }
}
