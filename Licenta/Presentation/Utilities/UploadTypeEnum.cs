﻿using System;
namespace Licenta.Presentation.Utilities
{
    public enum UploadType
    {
        PickPicture,
        TakePicture
    }
}
