﻿using System;
using System.Collections.Generic;
using DLToolkit.Forms.Controls;
using Licenta.Domain.Models;
using Licenta.Presentation.Utilities;
using Licenta.Presentation.ViewModels;
using Licenta.Presentation.Views;
using Licenta.Presentation.Views.ProductDetails;
using Licenta.Presentation.Views.Search;
using Licenta.Utilities;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Licenta
{
    public partial class App : Application
    {
        public static Action<string> PostSuccessFacebookAction { get; set; }
        public static int ScreenHeight { get; set; }
        public static int ScreenWidth { get; set; }

        public App()
        {
            InitializeComponent();

            FlowListView.Init();
            DependencyResolver.Configure();

            var isUserLoggedIn = !Preferences.Get(Constants.RestStrings.Token, string.Empty).IsNullOrEmpty();
            MainPage = isUserLoggedIn ? GetRecentSearchesPage : GetLoginPage;
        }

        public Page GetRecentSearchesPage => new RootPage();

        public Page GetLoginPage => new LoginPage();

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
