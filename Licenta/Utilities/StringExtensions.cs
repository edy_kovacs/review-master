﻿using System.Linq;

namespace Licenta.Utilities
{
    public static class StringExtensions
    {
        public static bool IsNullOrEmpty(this string s)
        {
            return string.IsNullOrEmpty(s);
        }

        public static bool IsNullOrWhiteSpace(this string s)
        {
            return string.IsNullOrWhiteSpace(s);
        }

        public static string FirstCharToUpper(this string input)
        {
            return input == null || input.IsNullOrEmpty() ?
                input : input.First().ToString().ToUpper() + input.Substring(1);
        }
    }
}
