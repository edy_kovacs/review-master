﻿using System;
using Xamarin.Forms;

namespace Licenta.Utilities
{
    public static class BindableObjectExtensions
    {
        /// <summary>
        /// Used to avoid null checks and equality checks when handling a bindable property change
        /// </summary>
        /// <param name="nullCheck">If set to <c>true</c> null check the new value.</param>
        /// <param name="equalityCheck">If set to <c>true</c> equality check for old and new value.</param>
        public static void ChangeBindableProperty<T>(this BindableObject bindable, object oldValue, object newValue, Action<T> propertyChangedHandler,
                                                  bool nullCheck = true, bool equalityCheck = true) where T : BindableObject
        {
            var isNewValueNull = newValue == null && nullCheck;
            var areValuesEqual = Equals(oldValue, newValue) && equalityCheck;
            if (bindable != null && !isNewValueNull && !areValuesEqual)
            {
                propertyChangedHandler?.Invoke(bindable as T);
            }
        }
    }
}
