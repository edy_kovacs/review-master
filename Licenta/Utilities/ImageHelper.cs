﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Licenta.Utilities
{
    public static class ImageHelper
    {
        #region Private Members

        private static HttpClient _httpClient;

        private static readonly Dictionary<byte[], Func<BinaryReader, Size>> ImageDecoders = new Dictionary<byte[], Func<BinaryReader, Size>>()
        {
            { new byte[] { 0x42, 0x4D }, DecodeBitmap },
            { new byte[] { 0x47, 0x49, 0x46, 0x38, 0x37, 0x61 }, DecodeGif },
            { new byte[] { 0x47, 0x49, 0x46, 0x38, 0x39, 0x61 }, DecodeGif },
            { new byte[] { 0xff, 0xd8 }, DecodeJfif },
            { new byte[] { 0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A }, DecodePng }
        };

        private static readonly Dictionary<string, string> ImageFileExtensions = new Dictionary<string, string>()
        {
            { "image/bmp", "bmp" },
            { "image/gif", "gif" },
            { "image/jpeg", "jpg" },
            { "image/png", "png" }
        };

        private static readonly Dictionary<byte[], string> ImageMimeTypes = new Dictionary<byte[], string>()
        {
            { new byte[] { 0x42, 0x4D }, "image/bmp" },
            { new byte[] { 0x47, 0x49, 0x46, 0x38, 0x37, 0x61 }, "image/gif" },
            { new byte[] { 0x47, 0x49, 0x46, 0x38, 0x39, 0x61 }, "image/gif" },
            { new byte[] { 0xff, 0xd8 }, "image/jpeg" },
            { new byte[] { 0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A }, "image/png" }
        };

        #endregion

        static ImageHelper()
        {
            // ====================================================================================================
            // Use this code for non-Xamarin apps (console apps, etc)
            // ====================================================================================================
            // Instantiate a HttpClient with caching disabled (to prevent partial range-requests from being cached and affecting subsequent requests)
            _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (compatible; MySuperTool)");
            _httpClient.DefaultRequestHeaders.Add("Cache-Control", "no-cache, no-store");

            // ====================================================================================================
            // Use this code for Xamarin apps and comment out the lines above
            // NativeMessageHandler is part of the ModernHttpClient NuGet package
            // ====================================================================================================
            // Instantiate a HttpClient with caching disabled (to prevent partial range-requests from being cached and affecting subsequent requests)
            //_httpClient = new HttpClient(new NativeMessageHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate, DisableCaching = true });
            //_httpClient.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (compatible; MySuperTool)");
        }

        /// <summary>
        /// Gets the dimensions of an image from a byte stream.
        /// </summary>
        /// <returns>The dimensions of the image.</returns>
        /// <exception cref="ArgumentException">The image was of an unrecognized format.</exception>    
        public static Size GetDimensions(byte[] imageBytes)
        {
            var binaryReader = new BinaryReader(new MemoryStream(imageBytes));

            int maxMagicBytesLength = ImageDecoders.Keys.OrderByDescending(x => x.Length).First().Length;

            var magicBytes = new byte[maxMagicBytesLength];

            for (var i = 0; i < maxMagicBytesLength; i += 1)
            {
                magicBytes[i] = binaryReader.ReadByte();

                foreach (var kvp in ImageDecoders)
                {
                    if (magicBytes.StartsWith(kvp.Key))
                    {
                        return kvp.Value(binaryReader);
                    }
                }
            }

            throw new ArgumentException("Could not determine image mime type.", nameof(imageBytes));
        }

        /// <summary>
        /// Gets the dimensions of an image from a Uri.
        /// </summary>
        /// <returns>The dimensions of the image.</returns>
        /// <exception cref="ArgumentException">The image was of an unrecognized format.</exception>    
        public static Size GetDimensions(Uri uri)
        {
            var moreBytes = true;
            var chunkSize = 1024;
            var currentStart = 0;

            byte[] allBytes = { };

            while (moreBytes)
            {
                try
                {
                    var newBytes = GetSomeBytes(uri, currentStart, currentStart + chunkSize - 1);

                    if (newBytes.Length < chunkSize)
                    {
                        moreBytes = false;
                    }

                    allBytes = Combine(allBytes, newBytes);

                    return GetDimensions(allBytes);
                }
                catch (FileNotFoundException)
                {
                    throw;
                }
                catch
                {
                    if (currentStart >= 100000)
                    {
                        break;
                    }

                    currentStart += chunkSize;
                }
            }

            return new Size(0, 0);
        }

        #region Private Methods

        private static byte[] Combine(byte[] first, byte[] second)
        {
            var result = new byte[first.Length + second.Length];

            Buffer.BlockCopy(first, 0, result, 0, first.Length);
            Buffer.BlockCopy(second, 0, result, first.Length, second.Length);

            return result;
        }

        private static Size DecodeBitmap(BinaryReader binaryReader)
        {
            binaryReader.ReadBytes(16);

            var width = binaryReader.ReadInt32();
            var height = binaryReader.ReadInt32();

            return new Size(width, height);
        }

        private static Size DecodeGif(BinaryReader binaryReader)
        {
            var width = binaryReader.ReadInt16();
            var height = binaryReader.ReadInt16();

            return new Size(width, height);
        }

        private static Size DecodeJfif(BinaryReader binaryReader)
        {
            while (binaryReader.ReadByte() == 0xff)
            {
                var marker = binaryReader.ReadByte();
                var chunkLength = binaryReader.ReadLittleEndianInt16();

                if (marker == 0xc0 || marker == 0xc1 || marker == 0xc2)
                {
                    binaryReader.ReadByte();

                    var height = binaryReader.ReadLittleEndianInt16();
                    var width = binaryReader.ReadLittleEndianInt16();

                    return new Size(width, height);
                }

                binaryReader.ReadBytes(chunkLength - 2);
            }

            throw new ArgumentException("Could not read image data.");
        }

        private static Size DecodePng(BinaryReader binaryReader)
        {
            binaryReader.ReadBytes(8);

            var width = binaryReader.ReadLittleEndianInt32();
            var height = binaryReader.ReadLittleEndianInt32();

            return new Size(width, height);
        }

        private static byte[] GetSomeBytes(Uri uri, int startRange, int endRange)
        {
            var request = new HttpRequestMessage { RequestUri = uri };

            request.Headers.Range = new RangeHeaderValue(startRange, endRange);

            try
            {
                var response = _httpClient.SendAsync(request).Result;

                if (!response.IsSuccessStatusCode)
                {
                    throw new FileNotFoundException();
                }

                return response.Content.ReadAsByteArrayAsync().Result;
            }
            catch (FileNotFoundException)
            {
                throw;
            }
            catch
            {
                return new byte[] { };
            }
        }

        private static short ReadLittleEndianInt16(this BinaryReader binaryReader)
        {
            var bytes = new byte[sizeof(short)];

            for (var i = 0; i < sizeof(short); i += 1)
            {
                bytes[sizeof(short) - 1 - i] = binaryReader.ReadByte();
            }

            return BitConverter.ToInt16(bytes, 0);
        }

        private static int ReadLittleEndianInt32(this BinaryReader binaryReader)
        {
            var bytes = new byte[sizeof(int)];

            for (var i = 0; i < sizeof(int); i += 1)
            {
                bytes[sizeof(int) - 1 - i] = binaryReader.ReadByte();
            }

            return BitConverter.ToInt32(bytes, 0);
        }

        private static bool StartsWith(this byte[] thisBytes, byte[] thatBytes)
        {
            for (var i = 0; i < thatBytes.Length; i += 1)
            {
                if (thisBytes[i] != thatBytes[i])
                {
                    return false;
                }
            }

            return true;
        }

        #endregion
    }

    // Xamarin has a Size class in the Forms namespace, but it has limited functionality and is double-based, not int-based.  This class is ripped directly
    // from the online .NET Reference Source @ https://referencesource.microsoft.com/#System.Drawing/commonui/System/Drawing/Size.cs,a99f8f9f30730da4
    public struct Size
    {
        public static readonly Size Empty = new Size();

        private int _width;
        public int Width { get { return _width; } set { _width = value; } }

        private int _height;
        public int Height { get { return _height; } set { _height = value; } }

        public double AspectRatio { get { return (double)_width / _height; } }

        public Size(int width, int height)
        {
            _width = width;
            _height = height;
        }

        public Size(double width, double height)
        {
            _width = (int)width;
            _height = (int)height;
        }

        public static Size operator +(Size size1, Size size2)
        {
            return Add(size1, size2);
        }

        public static Size operator -(Size size1, Size size2)
        {
            return Subtract(size1, size2);
        }

        public static bool operator ==(Size size1, Size size2)
        {
            return size1.Width == size2.Width && size1.Height == size2.Height;
        }

        public static bool operator !=(Size size1, Size size2)
        {
            return !(size1 == size2);
        }

        public static Size Add(Size size1, Size size2)
        {
            return new Size(size1.Width + size2.Width, size1.Height + size2.Height);
        }

        public static Size Subtract(Size size1, Size size2)
        {
            return new Size(size1.Width - size2.Width, size1.Height - size2.Height);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Size))
            {
                return false;
            }

            var comp = (Size)obj;

            return (comp.Width == _width) && (comp.Height == _height);
        }

        public bool IsEmpty { get { return _width == 0 && _height == 0; } }

        public override int GetHashCode()
        {
            return _width ^ _height;
        }

        public override string ToString()
        {
            return "{Width=" + _width.ToString(CultureInfo.CurrentCulture) + ", Height=" + _height.ToString(CultureInfo.CurrentCulture) + "}";
        }
    }
}
