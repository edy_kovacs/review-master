﻿using System;
using System.Reflection;

namespace Licenta.Utilities
{
    public static class ObjectExtensions
    {
        public static object GetPropertyValue(this object @object, string propertyName)
        {
            var typeInfo = @object.GetType().GetTypeInfo();

            var propertyInfo = typeInfo.GetDeclaredProperty(propertyName);
            if (propertyInfo == null)
                throw new Exception($"Property {propertyName} not found on {typeInfo.Name} type.");

            return propertyInfo.GetValue(@object);
        }
    }
}
