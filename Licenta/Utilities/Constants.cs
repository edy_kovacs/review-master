﻿using Xamarin.Forms;

namespace Licenta.Utilities
{
    public static class Constants
    {
        public static class Colors
        {
            public static readonly Color LightGreySeparator = (Color)Application.Current.Resources["LightGreySeparator"];
            public static readonly Color LightGrey = (Color)Application.Current.Resources["LightGrey"];
            public static readonly Color CustomTeal = (Color)Application.Current.Resources["CustomTeal"];
            public static readonly Color PrimaryBlue = (Color)Application.Current.Resources["PrimaryBlue"];
            public static readonly Color DarkGrey = (Color)Application.Current.Resources["DarkGrey"];
            public static readonly Color DarkTeal = (Color)Application.Current.Resources["DarkTeal"];
            public static readonly Color MidGrey = (Color)Application.Current.Resources["MidGrey"];
            public static readonly Color LightestGrey = (Color)Application.Current.Resources["LightestGrey"];
            public static readonly Color LighterGrey = (Color)Application.Current.Resources["LighterGrey"];
            public static readonly Color GreenishBlack = (Color)Application.Current.Resources["GreenishBlack"];
            public static readonly Color WhiteLowOpacity = (Color)Application.Current.Resources["WhiteLowOpacity"];
        }

        public static class RestStrings
        {
            public const string AuthorizationHeader = "x-auth";
            public static readonly string Token = "x-auth";
            public static readonly string LoggedInUser = "loggedInUser";
        }

        public static class Strings
        {
            public static string AreYouSureText = "Are you sure?";
            public static string LogoutText = "You're about to log out.";
            public static string YesText = "Yes";
            public static string NoText = "No";
        }
    }
}
