﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Licenta.Data.Models;
using Licenta.Presentation.Utilities;
using Newtonsoft.Json;
using Plugin.Media.Abstractions;
using Xamarin.Forms;

namespace Licenta.Utilities
{
    public static class OcrService
    {
        private static int _defaultResizeWidth = 768;
        private static int _defaultResizeHeight = 1024;

        public static async Task<List<string>> GetOcrResultAsync(MediaFile _image, string ocrLanguage, bool shouldResize, int width, int height, IAlertService Alert)
        {
            if (_image == null)
            {
                return null;
            }

            var httpClient = new HttpClient();

            httpClient.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", "85808b6cc4b641669dbac6057e7b1fb8");

            var url = $"https://westeurope.api.cognitive.microsoft.com/vision/v2.0/ocr?language={ocrLanguage}";

            using (var memoryStream = new MemoryStream())
            {
                _image.GetStream().CopyTo(memoryStream);
                _image.Dispose();

                byte[] byteData = memoryStream.ToArray();

                if (shouldResize)
                {
                    byteData = DependencyService.Get<IMediaService>().ResizeImage(byteData, width, height);
                }
                else
                {
                    var imageSize = ImageHelper.GetDimensions(byteData);

                    if (imageSize.Height > 3000 || imageSize.Width > 3000)
                    {
                        byteData = DependencyService.Get<IMediaService>().ResizeImage(byteData, _defaultResizeWidth, _defaultResizeHeight);
                    }
                }

                using (ByteArrayContent content = new ByteArrayContent(byteData))
                {
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                    var responseMsg = await httpClient.PostAsync(url, content);

                    if (responseMsg.IsSuccessStatusCode)
                    {
                        var responseContent = await responseMsg.Content.ReadAsStringAsync();
                        var receivedResponse = JsonConvert.DeserializeObject<AzureResponseDataModel>(responseContent);
                        var wordsResult = GetWordsFromResponse(receivedResponse);

                        return wordsResult;
                    }
                    else
                    {
                        var errorMessage = responseMsg.Content.ReadAsStringAsync().Result;

                        if (errorMessage.Contains("too large"))
                        {
                            await Alert.ShowAsync("Error - Photo too large", "The photo you are trying to upload is too large. Please make sure that the resize option is checked and try again", "Ok");
                        }
                        else
                        {
                            await Alert.ShowAsync("We are very sorry..", "Something unnexpected happened. Please try again later", "Ok");
                        }
                    }
                }
            }

            return null;
        }

        private static List<string> GetWordsFromResponse(AzureResponseDataModel receivedResponse)
        {
            var words = new List<string>();

            foreach (var region in receivedResponse.Regions)
            {
                foreach (var line in region.Lines)
                {
                    foreach (var word in line.Words)
                    {
                        words.Add(word.Text);
                    }
                }
            }

            return words;
        }
    }
}
