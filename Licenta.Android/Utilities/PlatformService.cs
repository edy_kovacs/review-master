﻿using Licenta.Domain.Utilities;
using Licenta.Droid.Utilities;

[assembly: Xamarin.Forms.Dependency(typeof(PlatformService))]
namespace Licenta.Droid.Utilities
{
    public class PlatformService : IPlatformService
    {
        public char GetDirectoryPathSeparator()
        {
            return System.IO.Path.DirectorySeparatorChar;
        }
    }
}
