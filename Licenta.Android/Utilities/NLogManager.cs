﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Licenta.Droid.Utilities;
using Licenta.Domain.Utilities;
using NLog;
using NLog.Config;
using NLog.Targets;
using NLog.Targets.Wrappers;
using Xamarin.Forms;

[assembly: Dependency(typeof(NLogManager))]
namespace Licenta.Droid.Utilities
{
    public class NLogManager : ILogManager
    {
        private static string logLayout = "${longdate} [${level:uppercase=true}] ${message}";
        private static long archiveAboveSize = 1024 * 1024 * 512;
        private static int maxArchiveFiles = 2;
        private static int logQueueLimit = 5000;

        public NLogManager()
        {
            //TODO This should eventually be configured in PCL (https://github.com/NLog/NLog/issues/155)
            //or maybe replace it with https://github.com/serilog/serilog-sinks-xamarin
            var config = new LoggingConfiguration();

            var consoleTarget = new ConsoleTarget();
            consoleTarget.Layout = logLayout;
            config.AddTarget("console", consoleTarget);

            var consoleLogLevel = LogLevel.Info;
#if DEBUG
            consoleLogLevel = LogLevel.Trace;
#endif
            var consoleRule = new LoggingRule("*", consoleLogLevel, consoleTarget);
            config.LoggingRules.Add(consoleRule);

            var fileTarget = new FileTarget();
            fileTarget.Layout = logLayout;
            fileTarget.FileName = GetLogsFilePath();
            fileTarget.ArchiveAboveSize = archiveAboveSize;
            fileTarget.ArchiveNumbering = ArchiveNumberingMode.Rolling;
            fileTarget.MaxArchiveFiles = maxArchiveFiles;
            fileTarget.EnableArchiveFileCompression = true;
            var wrapper = new AsyncTargetWrapper(fileTarget, logQueueLimit, AsyncTargetWrapperOverflowAction.Discard);
            config.AddTarget("file", wrapper);

            var fileRule = new LoggingRule("*", LogLevel.Info, wrapper);
            config.LoggingRules.Add(fileRule);

            LogManager.Configuration = config;

            Debug.WriteLine($"Log file path: {fileTarget.FileName}");
        }

        public Licenta.Domain.Utilities.ILogger GetLog()
        {
            var logger = LogManager.GetCurrentClassLogger();
            return new NLogLogger(logger);
        }

        private string GetLogsFilePath()
        {
            string folder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string fullPath = Path.Combine(folder, "DeviceManagerLog.txt");

            return fullPath;
        }

        public string GetLogsFileContent()
        {
            var filePath = GetLogsFilePath();
            var fileContent = System.IO.File.ReadAllLines(filePath).TakeLast(1000);
            return string.Join(Environment.NewLine, fileContent);
        }
    }
}
