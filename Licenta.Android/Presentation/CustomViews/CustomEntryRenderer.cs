﻿using Android.Content;
using Android.Content.Res;
using Android.Graphics.Drawables;
using Android.Text;
using Licenta.Droid.Presentation.CustomViews;
using Licenta.Presentation.Views.CustomViews;
using Licenta.Utilities;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace Licenta.Droid.Presentation.CustomViews
{
    public class CustomEntryRenderer : EntryRenderer
    {
        public CustomEntryRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control == null || e.NewElement == null)
            {
                return;
            }

            Control.Background = null;

            if (e.NewElement is CustomEntry customEntry && !customEntry.HasBorder)
            {
                GradientDrawable gradientDrawable = new GradientDrawable();
                gradientDrawable.SetColor(global::Android.Graphics.Color.Transparent);
                this.Control.SetBackground(gradientDrawable);
                Control.SetHintTextColor(customEntry.PlaceholderColor.ToAndroid());
            }
        }
    }
}
