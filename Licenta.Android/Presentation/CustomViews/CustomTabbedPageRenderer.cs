﻿using System;
using System.ComponentModel;
using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.Graphics;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Views;
using Licenta.Droid.Presentation.CustomViews;
using Licenta.Presentation.Views.CustomViews;
using Licenta.Utilities;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomTabbedPage), typeof(CustomTabbedPageRenderer))]
namespace Licenta.Droid.Presentation.CustomViews
{
    public class CustomTabbedPageRenderer : Xamarin.Forms.Platform.Android.AppCompat.TabbedPageRenderer, BottomNavigationView.IOnNavigationItemSelectedListener
    {
        private CustomTabbedPage _page;
        private int _lastSelectedTabId = 0;

        public CustomTabbedPageRenderer(Context context) : base(context)
        {
            SetWillNotDraw(false);
        }

        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);

            Paint paint = new Paint();
            paint.AntiAlias = true;
            paint.Color = Constants.Colors.LightGreySeparator.ToAndroid();
            paint.SetStyle(Paint.Style.FillAndStroke);

            var desiredHeightForSeparatorInPixels = 1;
            var displayPixelDensity = (Context as Activity).Resources.DisplayMetrics.Density;
            var calculatedHeight = desiredHeightForSeparatorInPixels * displayPixelDensity;

            try
            {
                var tabbedPageNativeLayout = (Android.Widget.RelativeLayout)GetChildAt(0);
                var bottomNavigationBar = tabbedPageNativeLayout.GetChildAt(1);
                var bottomNavigationBarHeight = bottomNavigationBar.Height;

                var startDrawY = canvas.Height - bottomNavigationBarHeight;
                var stopDrawY = startDrawY + calculatedHeight;
                canvas.DrawRect(0, startDrawY, canvas.Width, stopDrawY, paint);
            }
            catch (NullReferenceException e)
            {
                System.Diagnostics.Debug.WriteLine("Custom TabbedPage Separator drawing failed");
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
        }

        protected override void OnElementChanged(ElementChangedEventArgs<TabbedPage> e)
        {
            base.OnElementChanged(e);
            if (e.NewElement != null)
            {
                _page = (CustomTabbedPage)e.NewElement;
            }
            else
            {
                _page = (CustomTabbedPage)e.OldElement;
            }
        }

        bool BottomNavigationView.IOnNavigationItemSelectedListener.OnNavigationItemSelected(IMenuItem item)
        {
            if (_lastSelectedTabId == item.ItemId)
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await _page.CurrentPage.Navigation.PopToRootAsync();
                });
            }
            else
            {
                _page.CurrentPage = _page.Children[item.ItemId];
            }

            _lastSelectedTabId = item.ItemId;
            return true;
        }
    }
}
