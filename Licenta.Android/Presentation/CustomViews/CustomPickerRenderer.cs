﻿using System;
using Android.Content;
using Android.Graphics.Drawables;
using Licenta.Droid.Presentation.CustomViews;
using Licenta.Presentation.Views.CustomViews;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomPicker), typeof(CustomPickerRenderer))]
namespace Licenta.Droid.Presentation.CustomViews
{
    public class CustomPickerRenderer : PickerRenderer
    {
        public CustomPickerRenderer(Context ctx) : base(ctx)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement == null)
            {
                GradientDrawable gd = new GradientDrawable();
                gd.SetStroke(0, Android.Graphics.Color.Transparent);
                Control.SetBackground(gd);
            }
        }
    }
}
