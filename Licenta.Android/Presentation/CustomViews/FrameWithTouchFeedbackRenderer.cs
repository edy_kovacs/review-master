﻿using Android.Content;
using Android.Views;
using Licenta.Droid.Presentation.CustomViews;
using Licenta.Presentation.Views.CustomViews;

[assembly: Xamarin.Forms.ExportRenderer(typeof(FrameWithTouchFeedback), typeof(FrameWithTouchFeedbackRenderer))]
namespace Licenta.Droid.Presentation.CustomViews
{
    public class FrameWithTouchFeedbackRenderer : Xamarin.Forms.Platform.Android.AppCompat.FrameRenderer
    {
        public FrameWithTouchFeedbackRenderer(Context context) : base(context) { }

        public override bool OnTouchEvent(MotionEvent e)
        {
            base.OnTouchEvent(e);
            switch (e.Action)
            {
                case MotionEventActions.Down:
                    UpdateBackgroundColor(shouldHighlight: true);
                    break;
                case MotionEventActions.Move:
                    break;
                default:
                    UpdateBackgroundColor(shouldHighlight: false);
                    break;
            }

            return true;
        }

        private void UpdateBackgroundColor(bool shouldHighlight)
        {
            var frameView = Element as FrameWithTouchFeedback;

            if (frameView == null) { return; }
            var color = frameView.PreferredBackgroundColor;
            if (shouldHighlight)
            {
                color = frameView.PreferredHighlightColor;
            }
            Element.BackgroundColor = color;
        }
    }
}
