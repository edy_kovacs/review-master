﻿using System;
using Android.App;
using Android.Content;
using Licenta.Droid.Presentation.CustomViews;
using Licenta.Presentation.Views.CustomViews;
using Xamarin.Facebook;
using Xamarin.Facebook.Login;
using Xamarin.Facebook.Login.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(FacebookLoginButton), typeof(FacebookLoginButtonRenderer))]
namespace Licenta.Droid.Presentation.CustomViews
{
    public class FacebookLoginButtonRenderer : ButtonRenderer
    {
        public FacebookLoginButtonRenderer(Context context) : base(context)
        {

        }

        private static Activity _activity;

        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);

            _activity = this.Context as MainActivity;
            var loginButton = (Button)Element;
            var facebookCallback = new FacebookCallback<LoginResult>
            {
                HandleSuccess = shareResult =>
                {
                    Action<string> local = App.PostSuccessFacebookAction;
                    if (local != null)
                    {
                        local(shareResult.AccessToken.Token);
                        LoginManager.Instance.LogOut();
                    }
                }
,
                HandleCancel = () =>
                {
                    Console.WriteLine("HelloFacebook: Canceled");
                },
                HandleError = shareError =>
                {
                    Console.WriteLine("HelloFacebook: Error: {0}", shareError);
                }
            };

            LoginManager.Instance.RegisterCallback(MainActivity.CallbackManager, facebookCallback);

            if (loginButton != null)
            {
                loginButton.Clicked += LoginButton_Click;
            }
        }

        void LoginButton_Click(object sender, EventArgs e)
        {
            LoginManager.Instance.LogInWithReadPermissions(_activity, new string[] { "email", "public_profile" });
        }
    }
}

