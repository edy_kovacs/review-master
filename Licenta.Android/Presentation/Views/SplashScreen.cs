﻿using System;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Support.V7.App;

namespace Licenta.Droid.Presentation.Views
{
    [Activity(Theme = "@style/Theme.Splash", MainLauncher = true, NoHistory = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class SplashScreen : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            this.StartActivity(typeof(MainActivity));
            Finish();
        }
    }
}
