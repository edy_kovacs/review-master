﻿using Licenta.Domain.Utilities;
using Licenta.iOS.Utilities.DependencyServices;

[assembly: Xamarin.Forms.Dependency(typeof(PlatformService))]
namespace Licenta.iOS.Utilities.DependencyServices
{
    public class PlatformService : IPlatformService
    {
        public char GetDirectoryPathSeparator()
        {
            return System.IO.Path.DirectorySeparatorChar;
        }
    }
}
