﻿using System;
using Facebook.LoginKit;
using Foundation;
using Licenta.iOS.Presentation.CustomViews;
using Licenta.Presentation.Views.CustomViews;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(FacebookLoginButton), typeof(FacebookLoginButtonRenderer))]
namespace Licenta.iOS.Presentation.CustomViews
{
    public class FacebookLoginButtonRenderer : ButtonRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                var view = (Button)Element;

                if(view != null)
                {
                    view.Clicked += Clicked;
                }
            }
        }

        private void Clicked(object sender, EventArgs e)
        {
            var window = UIApplication.SharedApplication.KeyWindow;
            var vc = window.RootViewController;

            new LoginManager().LogInWithReadPermissions(new string[] { "email", "public_profile" }, vc, AuthCompleted);
        }

        private void AuthCompleted(LoginManagerLoginResult result, NSError error)
        {
            if(!result.IsCancelled)
            {
                App.PostSuccessFacebookAction(result.Token.TokenString);
                new LoginManager().LogOut();
            }
        }
    }
}
