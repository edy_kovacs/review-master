﻿using System;
using Licenta.iOS.Presentation.CustomViews;
using Licenta.Presentation.Views.CustomViews;
using UIKit;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;

[assembly: ExportRenderer(typeof(FrameWithTouchFeedback), typeof(FrameWithTouchFeedbackRenderer))]
namespace Licenta.iOS.Presentation.CustomViews
{
    public class FrameWithTouchFeedbackRenderer : FrameRenderer
    {
        public FrameWithTouchFeedbackRenderer()
        {
            UpdateBackgroundColor(false);
        }

        public override void TouchesBegan(Foundation.NSSet touches, UIEvent evt)
        {
            base.TouchesBegan(touches, evt);
            UpdateBackgroundColor(shouldHighlight: true);
        }

        public override void TouchesEnded(Foundation.NSSet touches, UIEvent evt)
        {
            base.TouchesEnded(touches, evt);
            UpdateBackgroundColor(shouldHighlight: false);
        }

        public override void TouchesCancelled(Foundation.NSSet touches, UIEvent evt)
        {
            base.TouchesCancelled(touches, evt);
            UpdateBackgroundColor(shouldHighlight: false);
        }

        private void UpdateBackgroundColor(bool shouldHighlight)
        {
            var frameView = Element as FrameWithTouchFeedback;
            if (frameView == null) { return; }

            var color = frameView.PreferredBackgroundColor.ToUIColor();
            if (shouldHighlight)
            {
                color = frameView.PreferredHighlightColor.ToUIColor();
            }
            NativeView.BackgroundColor = color;
        }
    }
}
