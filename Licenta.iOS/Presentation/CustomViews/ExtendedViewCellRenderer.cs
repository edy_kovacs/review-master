﻿using System;
using Licenta.iOS.Presentation.CustomViews;
using Licenta.Presentation.Views.CustomViews;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ExtendedViewCell), typeof(ExtendedViewCellRenderer))]
namespace Licenta.iOS.Presentation.CustomViews
{
    public class ExtendedViewCellRenderer : ViewCellRenderer
    {
        public override UITableViewCell GetCell(Cell item, UITableViewCell reusableCell, UITableView tv)
        {
            var cell = base.GetCell(item, null, tv);
            var view = item as ExtendedViewCell;

            cell.SelectedBackgroundView = new UIView
            {
                BackgroundColor = view.SelectedBackgroundColor.ToUIColor()
            };

            if (!view.IsSelectable)
            {
                cell.SelectionStyle = UITableViewCellSelectionStyle.None;
            }
            else
            {
                cell.SelectionStyle = UITableViewCellSelectionStyle.Gray;
            }

            return cell;
        }
    }
}
