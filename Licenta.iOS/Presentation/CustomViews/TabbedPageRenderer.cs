﻿using System;
using Licenta.iOS.Presentation.CustomViews;
using Licenta.Utilities;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(TabbedPage), typeof(TabbedPageRenderer))]
namespace Licenta.iOS.Presentation.CustomViews
{
    public class TabbedPageRenderer : TabbedRenderer
    {
        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);
            TabBar.TintColor = Constants.Colors.CustomTeal.ToUIColor();
            //TabBar.BarTintColor = Constants.Colors.CustomTeal.ToUIColor();
        }
    }
}
