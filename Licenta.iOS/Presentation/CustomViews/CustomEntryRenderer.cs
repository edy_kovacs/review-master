﻿using Facebook.LoginKit;
using Licenta.iOS.Presentation.CustomViews;
using Licenta.Presentation.Views.CustomViews;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace Licenta.iOS.Presentation.CustomViews
{
    public class CustomEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement == null && e.NewElement != null)
            {
                var entry = (CustomEntry)Element;

                if (entry != null && !entry.HasBorder && this.Control != null)
                {
                    this.Control.BorderStyle = UITextBorderStyle.None;
                }
            }
        }
    }
}
