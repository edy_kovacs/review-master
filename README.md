# Login/Register Page
<p align="center">
<img width="720" height="1280" src="screenshots/login.png">
</p>

# Recent Searches Page
<p align="center">
<img width="720" height="1280" src="screenshots/recents.png">
</p>


# Choose Search Type Page
<p align="center">
<img width="720" height="1280" src="screenshots/searchtype.png">
</p>


# Manual Search Page
<p align="center">
<img width="720" height="1280" src="screenshots/manual.png">
</p>


# Options available for OCR Search
<p align="center">
<img width="720" height="1280" src="screenshots/ocroptions.png">
</p>


# Profile Page
<p align="center">
<img width="720" height="1280" src="screenshots/profile.png">
</p>